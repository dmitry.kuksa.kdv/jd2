package com.dvkuksa.services;

import com.dvkuksa.model.dal.entity.MedicalReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MedicalReportService {

    Page<MedicalReport> findAll(Pageable page);

    MedicalReport findById(Long medicalReportId);

    Page<MedicalReport> findDoctorMedicalReportPage(Pageable page, Long userAccountId);

    Page<MedicalReport> findPatientMedicalReportPage(Pageable page, Long userAccountId);

    List<MedicalReport> findPatientMedicalReportList(Long patientId);

    MedicalReport save(MedicalReport medicalReport, Long doctorId, Long patientId);
}
