package com.dvkuksa.services;

import com.dvkuksa.model.dto.userinfo.UserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface UserInfoService {

    UserInfo getUserInfoByUserAccountId(Long userAccountId);

    UserInfo getUserInfoByUserId(Long userId);

    Page<UserInfo> getUserInfoPage(Pageable pageable);

    Page<UserInfo> getUserInfoPage(Pageable pageable, String roleCode);

    Page<UserInfo> getUserInfoPage(Pageable pageable, String roleCode, String fullName);

    Boolean isUserExist(String login);

    UserInfo getUserInfoByEmail(String email);
}
