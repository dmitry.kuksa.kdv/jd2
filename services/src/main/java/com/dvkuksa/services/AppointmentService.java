package com.dvkuksa.services;

import com.dvkuksa.model.dto.appointmentinfo.AppointmentInfo;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service("")
public interface AppointmentService {

    List<AppointmentInfo> findAppointmentInfoListByDate(LocalDate localDate);

    List<AppointmentInfo> findDoctorAppointmentListByDate(LocalDate localDate, Long doctorId);

    List<AppointmentInfo> findPatientAppointmentList(Long patientId);

    void saveAll(Long doctorId, List<LocalDateTime> localDateTimeList);

    void bookAnAppointment(Long appointmentId, Long patientId);
}
