package com.dvkuksa.services;

import com.dvkuksa.model.dal.entity.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {
    List<Role> getAllRoles();
}
