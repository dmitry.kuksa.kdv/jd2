package com.dvkuksa.services;

import com.dvkuksa.model.dto.userinfo.UserInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface AdministrationService {

    UserInfo registration(UserInfo userInfo, String roleCode, MultipartFile file);
    UserInfo update(UserInfo userInfo, String roleCode, MultipartFile file);
    Boolean activateUser(String activationCode);
    Boolean validateActivateCode(String activationCode);
    Boolean validatePassword(Long userId, String password);
    void updateUserPassword(String activationCode, String password);
    void updateUserPassword(Long userId, String password);
    void resetUserPassword(Long userId, String userAccountEmail);
}
