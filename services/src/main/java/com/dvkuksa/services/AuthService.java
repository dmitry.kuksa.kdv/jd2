package com.dvkuksa.services;


import com.dvkuksa.model.dal.entity.User;
import com.dvkuksa.model.dto.userinfo.UserInfo;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface AuthService extends UserDetailsService {

    User authenticate(String login, String password);

    UserInfo authorize(Long userAccountId);
}


