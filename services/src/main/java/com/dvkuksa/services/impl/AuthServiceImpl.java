package com.dvkuksa.services.impl;

import com.dvkuksa.model.dal.entity.User;
import com.dvkuksa.model.dal.repository.UserRepository;
import com.dvkuksa.model.dto.userinfo.UserInfo;
import com.dvkuksa.model.exception.InvalidDataException;
import com.dvkuksa.services.AuthService;
import com.dvkuksa.services.UserInfoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
public class AuthServiceImpl implements AuthService {

    private static final Logger LOGGER = LogManager.getLogger(AuthServiceImpl.class);

    private final UserInfoService userInfoService;
    private final UserRepository userRepository;

    public AuthServiceImpl(UserInfoService userInfoService,
                           UserRepository userRepository) {
        this.userInfoService = userInfoService;
        this.userRepository = userRepository;
    }

    @Override
    public User authenticate(String userLogin, String userPassword) throws InvalidDataException {
        return getUserByLoginAndPassword(userLogin, encodePassword(userLogin, userPassword));
    }

    @Override
    public UserInfo authorize(Long userId) {
        return userInfoService.getUserInfoByUserId(userId);
    }

    private String encodePassword(String salt, String password) {
        String stringToEncode = salt.concat(password);
        String encodedString;
        try {
            MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
            msdDigest.update(stringToEncode.getBytes("UTF-8"), 0, stringToEncode.length());
            encodedString = DatatypeConverter.printHexBinary(msdDigest.digest());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            LOGGER.error("Password encoding error", e);
            throw new InvalidDataException("Something Wrong", 0, (Throwable) e);
        }
        return encodedString;
    }

    private User getUserByLoginAndPassword(String login, String password) throws InvalidDataException {
        User user;
        user = userRepository.findByUserLoginAndUserPassword(login, password);
        if (user == null) {
            LOGGER.error("User not found");
            throw new InvalidDataException("Incorrect login or password", 401);
        }
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String userLogin) throws UsernameNotFoundException {
        User user = userRepository.findByUserLogin(userLogin);
        if (user == null) {
            LOGGER.error("User not found");
            throw new UsernameNotFoundException("Invalid login or password");
        }
        return userInfoService.getUserInfoByUserId(user.getId());
    }
}
