package com.dvkuksa.services.impl;

import com.dvkuksa.model.dal.entity.*;
import com.dvkuksa.model.dal.repository.*;
import com.dvkuksa.model.dto.userinfo.UserInfo;
import com.dvkuksa.model.dto.userinfo.UserInfoMapper;
import com.dvkuksa.model.exception.InvalidDataException;
import com.dvkuksa.services.AdministrationService;
import com.dvkuksa.services.MailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Service
public class AdministrationServiceImpl implements AdministrationService {

    private static final Logger LOGGER = LogManager.getLogger(AdministrationServiceImpl.class);

    private static final String MAIL_ACTIVATION_SUBJECT = "Account activation";
    private static final String MAIL_ACTIVATION_MESSAGE_TEMPLATE =
            "Hello, %s! \n Please, visit link: http://localhost:8080/activate/%s";
    private static final String MAIL_PASSWORD_RESET_SUBJECT = "Password reset";
    private static final String MAIL_PASSWORD_RESET_MESSAGE_TEMPLATE =
            "Hello, %s! \n Please, visit link: http://localhost:8080/passwordUpdate/%s to create new password";

    @Value("${upload.path}")
    private String uploadPath;

    private final UserRepository userRepository;
    private final UserAccountUserLinkRepository userAccountUserLinkRepository;
    private final UserAccountRepository userAccountRepository;
    private final UserAccountRoleLinkRepository userAccountRoleLinkRepository;
    private final RoleRepository roleRepository;
    private final UserInfoMapper userInfoMapper;
    private final MailService mailService;
    private final PasswordEncoder passwordEncoder;


    public AdministrationServiceImpl(UserRepository userRepository,
                                     UserAccountUserLinkRepository userAccountUserLinkRepository,
                                     UserAccountRepository userAccountRepository,
                                     UserAccountRoleLinkRepository userAccountRoleLinkRepository,
                                     RoleRepository roleRepository,
                                     UserInfoMapper userInfoMapper,
                                     MailService mailService,
                                     PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userAccountUserLinkRepository = userAccountUserLinkRepository;
        this.userAccountRepository = userAccountRepository;
        this.userAccountRoleLinkRepository = userAccountRoleLinkRepository;
        this.roleRepository = roleRepository;
        this.userInfoMapper = userInfoMapper;
        this.mailService = mailService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public UserInfo registration(UserInfo userInfo, String roleCode, MultipartFile file) throws InvalidDataException {
        try {
            UserAccount userAccount = saveUserAccount(userInfo, file);
            User user = saveUser(userInfo, passwordEncoder.encode(userInfo.getPassword()));
            saveUserAccountUserLink(userAccount, user);
            Role userAccountRole = roleRepository.findByRoleCode(roleCode);
            saveUserAccountRoleLink(userAccount, userAccountRole);
            userInfo = userInfoMapper.buildUserInfo(user, userAccount, userAccountRole);

            if (!StringUtils.isEmpty(userAccount.getUserAccountEmail())) {
                String message = String.format(MAIL_ACTIVATION_MESSAGE_TEMPLATE,
                        userAccount.getUserAccountName(),
                        user.getActivationCode()
                );
                mailService.send(userAccount.getUserAccountEmail(), MAIL_ACTIVATION_SUBJECT, message);
            }
            return userInfo;
        } catch (Exception e) {
            LOGGER.error("User registration error", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }
    }

    @Override
    @Transactional
    public UserInfo update(UserInfo userInfo, String roleCode, MultipartFile file) {
        try {
            UserAccount userAccount = userAccountRepository.findUserAccountByUserAccountId(userInfo.getUserAccountId());
            userAccount.setUserAccountName(userInfo.getLastName() + " "
                    + userInfo.getFirstName() + " " + userInfo.getMiddleName());
            userAccount.setUserAccountEmail(userInfo.getUserAccountEmail());
            userAccount.setUserAccountPhoneNumber(userInfo.getUserAccountPhoneNumber());
            try {
                saveFile(userAccount, file);
            } catch (IOException e) {
                LOGGER.error("Profile file save error", e);
                throw new InvalidDataException("Profile file save error", 0, e);
            }
            userAccountRepository.save(userAccount);

            User user = userRepository.findUserById(userInfo.getUserId());
            user.setLastName(userInfo.getLastName());
            user.setFirstName(userInfo.getFirstName());
            user.setMiddleName(userInfo.getMiddleName());
            user.setFullName(userInfo.getLastName() + " "
                    + userInfo.getFirstName() + " " + userInfo.getMiddleName());
            user.setIsEnabled(userInfo.getIsUserEnabled() == null ? false : userInfo.getIsUserEnabled());
            userRepository.save(user);

            userAccountRoleLinkRepository.delete(
                    userAccountRoleLinkRepository.findByUserAccountId(userAccount.getUserAccountId()));

            Role role = roleRepository.findByRoleCode(roleCode);

            saveUserAccountRoleLink(userAccount, role);
            userInfo = userInfoMapper.buildUserInfo(user, userAccount, role);
        } catch (Exception e) {
            LOGGER.error("User account update error", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }
        return userInfo;
    }

    @Override
    public Boolean activateUser(String activationCode) {
        try {
            User user = userRepository.findByActivationCode(activationCode);
            if (user == null) {
                LOGGER.error("Activation. User not found");
                return false;
            }
            user.setActivationCode(null);
            user.setIsEnabled(true);
            userRepository.save(user);
            return true;
        } catch (Exception e) {
            LOGGER.error("User activation error", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }
    }

    @Override
    public Boolean validateActivateCode(String activationCode) {
        User user = userRepository.findByActivationCode(activationCode);
        if (user == null) {
            LOGGER.error("Activation. User not found");
            throw new InvalidDataException("User activation code not valid", 0);
        }
        return true;
    }

    @Override
    public Boolean validatePassword(Long userId, String password) {
        User user = userRepository.findUserById(userId);
        Boolean validationResult;
        try {
            validationResult = passwordEncoder.matches(password, user.getUserPassword());
        } catch (Exception e) {
            LOGGER.error("Password validation error", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }
        return validationResult;
    }

    @Override
    public void updateUserPassword(String activationCode, String password) {
        try {
            User user = userRepository.findByActivationCode(activationCode);
            if (user == null) {
                LOGGER.error("Password update. User not found");
                throw new InvalidDataException("User activation code not valid", 0);
            }
            user.setUserPassword(passwordEncoder.encode(password));
            userRepository.save(user);
        } catch (Exception e) {
            LOGGER.error("Password change error", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }
    }

    @Override
    public void updateUserPassword(Long userId, String password) {
        try {
            User user = userRepository.findUserById(userId);
            if (user == null) {
                LOGGER.error("\"Password update.. User not found");
                throw new InvalidDataException("User not found", 0);
            }
            user.setUserPassword(passwordEncoder.encode(password));
            userRepository.save(user);
        } catch (Exception e) {
            LOGGER.error("Password change error", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }
    }

    @Override
    public void resetUserPassword(Long userId, String userAccountEmail) {
        try {
            User user = userRepository.findUserById(userId);
            user.setActivationCode(UUID.randomUUID().toString());
            user.setUserPassword("");
            user.setIsEnabled(false);
            userRepository.save(user);
            if (!StringUtils.isEmpty(userAccountEmail)) {
                String message = String.format(MAIL_PASSWORD_RESET_MESSAGE_TEMPLATE,
                        user.getFullName(),
                        user.getActivationCode());
                mailService.send(userAccountEmail, MAIL_PASSWORD_RESET_SUBJECT, message);
            } else {
                LOGGER.error("Password reset error. User email not found");
                throw new InvalidDataException("Something Wrong", 0);
            }
        } catch (Exception e) {
            LOGGER.error("Password reset error", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }

    }

    private void saveUserAccountRoleLink(UserAccount userAccount, Role userAccountRole) {
        UserAccountRoleLink userAccountRoleLink = new UserAccountRoleLink();
        userAccountRoleLink.setUserAccountId(userAccount.getUserAccountId());
        userAccountRoleLink.setRoleId(userAccountRole.getRoleId());
        userAccountRoleLinkRepository.save(userAccountRoleLink);
    }

    private void saveUserAccountUserLink(UserAccount userAccount, User user) {
        UserAccountUserLink userAccountUserLink = new UserAccountUserLink();
        userAccountUserLink.setUserId(user.getId());
        userAccountUserLink.setUserAccountId(userAccount.getUserAccountId());
        userAccountUserLinkRepository.save(userAccountUserLink);
    }

    private User saveUser(UserInfo userInfo, String userPassword) {
        User user = new User();
        user.setUserLogin(userInfo.getUserLogin());
        user.setUserPassword(userPassword);
        user.setLastName(userInfo.getLastName());
        user.setFirstName(userInfo.getFirstName());
        user.setMiddleName(userInfo.getMiddleName());
        user.setFullName(userInfo.getLastName() + " "
                + userInfo.getFirstName() + " " + userInfo.getMiddleName());
        user.setIsEnabled(userInfo.getIsUserEnabled() == null ? false : userInfo.getIsUserEnabled());
        user.setActivationCode(UUID.randomUUID().toString());
        user = userRepository.save(user);
        return user;
    }

    private UserAccount saveUserAccount(UserInfo userInfo, MultipartFile file) {
        UserAccount userAccount = new UserAccount();
        userAccount.setUserAccountName(userInfo.getLastName() + " "
                + userInfo.getFirstName() + " " + userInfo.getMiddleName());
        userAccount.setUserAccountEmail(userInfo.getUserAccountEmail());
        userAccount.setUserAccountPhoneNumber(userInfo.getUserAccountPhoneNumber());
        try {
            saveFile(userAccount, file);
        } catch (IOException e) {
            LOGGER.error("Profile file save error", e);
            throw new InvalidDataException("Profile file save error", 0, e);
        }
        userAccount = userAccountRepository.save(userAccount);
        return userAccount;
    }

    private void saveFile(UserAccount userAccount, MultipartFile file) throws IOException {
        if (file != null && !file.getOriginalFilename().isEmpty()) {
            File uploadDir = new File(uploadPath);

            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String uploadFileName = UUID.randomUUID().toString() + "." + file.getOriginalFilename();
            file.transferTo(new File(uploadPath + "/" + uploadFileName));

            userAccount.setProfileIconName(uploadFileName);
        }
    }
}
