package com.dvkuksa.services.impl;

import com.dvkuksa.model.dal.entity.Appointment;
import com.dvkuksa.model.dal.repository.AppointmentScheduleRepository;
import com.dvkuksa.model.dal.repository.UserAccountRepository;
import com.dvkuksa.model.dto.appointmentinfo.AppointmentInfo;
import com.dvkuksa.model.dto.appointmentinfo.AppointmentInfoMapper;
import com.dvkuksa.model.exception.InvalidDataException;
import com.dvkuksa.services.AppointmentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("appointmentService")
public class AppointmentServiceImpl implements AppointmentService {

    private static final Logger LOGGER = LogManager.getLogger(AppointmentServiceImpl.class);

    private final UserAccountRepository userAccountRepository;
    private final AppointmentScheduleRepository appointmentScheduleRepository;

    public AppointmentServiceImpl(UserAccountRepository userAccountRepository,
                                  AppointmentScheduleRepository appointmentScheduleRepository) {
        this.userAccountRepository = userAccountRepository;
        this.appointmentScheduleRepository = appointmentScheduleRepository;
    }

    @Override
    public List<AppointmentInfo> findAppointmentInfoListByDate(LocalDate localDate) {
        LocalDateTime dateFrom = localDate.atStartOfDay();
        LocalDateTime dateTo = localDate.plusDays(1).atStartOfDay();
        List<Appointment> appointmentList = appointmentScheduleRepository
                .findAllByAppointmentDateBetween(dateFrom, dateTo);

        return appointmentList.stream()
                .map(AppointmentInfoMapper::buildAppointmentInfo)
                .sorted(Comparator.comparing(AppointmentInfo::getId))
                .collect(Collectors.toList());
    }

    @Override
    public List<AppointmentInfo> findDoctorAppointmentListByDate(LocalDate localDate, Long doctorId) {
        LocalDateTime dateFrom = localDate.atStartOfDay();
        LocalDateTime dateTo = localDate.plusDays(1).atStartOfDay();
        List<Appointment> appointmentList = appointmentScheduleRepository
                .findAllByAppointmentDateBetweenAndDoctorUserAccountId(dateFrom, dateTo, doctorId);

        return appointmentList.stream()
                .map(AppointmentInfoMapper::buildAppointmentInfo)
                .sorted(Comparator.comparing(AppointmentInfo::getId))
                .collect(Collectors.toList());
    }

    @Override
    public List<AppointmentInfo> findPatientAppointmentList(Long patientId) {
        List<Appointment> appointmentList = appointmentScheduleRepository.findAllByPatientUserAccountId(patientId);
        return appointmentList.stream()
                .map(AppointmentInfoMapper::buildAppointmentInfo)
                .sorted(Comparator.comparing(AppointmentInfo::getId))
                .collect(Collectors.toList());
    }

    @Override
    public void saveAll(Long doctorId, List<LocalDateTime> localDateTimeList) {
        try {
            List<Appointment> appointmentList = localDateTimeList.stream()
                    .map(l -> buildNewAppointment(doctorId, l)).collect(Collectors.toList());
            appointmentScheduleRepository.saveAll(appointmentList);
        } catch (Exception e) {
            LOGGER.error("Appointment list save error", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }
    }

    @Override
    public void bookAnAppointment(Long appointmentId, Long patientId) {
        try {
            Appointment appointment = appointmentScheduleRepository.findAppointmentScheduleById(appointmentId);
            appointment.setPatient(userAccountRepository.findUserAccountByUserAccountId(patientId));
            appointment.setIsRegistered(true);
            appointmentScheduleRepository.save(appointment);
        } catch (Exception e) {
            LOGGER.error("Appointment booking error", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }
    }

    private Appointment buildNewAppointment(Long doctorId, LocalDateTime localDateTime) {
        Appointment appointment = new Appointment();
        appointment.setDoctor(userAccountRepository.findUserAccountByUserAccountId(doctorId));
        appointment.setAppointmentDate(localDateTime);
        appointment.setIsRegistered(false);
        appointment.setStatus("OPEN");
        return appointment;
    }

}
