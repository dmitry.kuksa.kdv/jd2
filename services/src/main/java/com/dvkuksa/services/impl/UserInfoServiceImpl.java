package com.dvkuksa.services.impl;

import com.dvkuksa.model.dal.entity.*;
import com.dvkuksa.model.dal.repository.*;
import com.dvkuksa.model.dto.userinfo.UserInfo;
import com.dvkuksa.model.dto.userinfo.UserInfoMapper;
import com.dvkuksa.model.exception.InvalidDataException;
import com.dvkuksa.services.UserInfoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    private static final Logger LOGGER = LogManager.getLogger(UserInfoServiceImpl.class);

    private final UserRepository userRepository;
    private final UserAccountUserLinkRepository userAccountUserLinkRepository;
    private final UserAccountRepository userAccountRepository;
    private final UserAccountRoleLinkRepository userAccountRoleLinkRepository;
    private final RoleRepository roleRepository;
    private final UserInfoMapper userInfoMapper;

    public UserInfoServiceImpl(UserRepository userRepository,
                               UserAccountUserLinkRepository userAccountUserLinkRepository,
                               UserAccountRepository userAccountRepository,
                               UserAccountRoleLinkRepository userAccountRoleLinkRepository,
                               RoleRepository roleRepository,
                               UserInfoMapper userInfoMapper) {
        this.userRepository = userRepository;
        this.userAccountUserLinkRepository = userAccountUserLinkRepository;
        this.userAccountRepository = userAccountRepository;
        this.userAccountRoleLinkRepository = userAccountRoleLinkRepository;
        this.roleRepository = roleRepository;
        this.userInfoMapper = userInfoMapper;
    }

    @Override
    public UserInfo getUserInfoByUserAccountId(Long userAccountId) throws InvalidDataException {
        try {
            UserAccount userAccount = userAccountRepository.findUserAccountByUserAccountId(userAccountId);
            Long userId = userAccountUserLinkRepository
                    .findByUserAccountId(userAccount.getUserAccountId())
                    .getUserId();
            User user = userRepository.findUserById(userId);
            Long roleId = userAccountRoleLinkRepository.findByUserAccountId(userAccountId).getRoleId();
            Role role = roleRepository.findRoleByRoleId(roleId);

            return userInfoMapper.buildUserInfo(user, userAccount, role);
        } catch (Exception e) {
            LOGGER.error("Can not build userInfo", e);
            throw new InvalidDataException("Something Wrong", 0, (Throwable) e);
        }
    }

    @Override
    public UserInfo getUserInfoByUserId(Long userId) throws InvalidDataException {
        try {
            User user = userRepository.findUserById(userId);
            Long userAccountId = userAccountUserLinkRepository
                    .findByUserId(user.getId())
                    .getUserAccountId();
            UserAccount userAccount = userAccountRepository.findUserAccountByUserAccountId(userAccountId);
            Long roleId = userAccountRoleLinkRepository.findByUserAccountId(userAccountId).getRoleId();
            Role role = roleRepository.findRoleByRoleId(roleId);

            return userInfoMapper.buildUserInfo(user, userAccount, role);
        } catch (Exception e) {
            LOGGER.error("Can not build userInfo", e);
            throw new InvalidDataException("Something Wrong", 0, (Throwable) e);
        }
    }

    @Override
    public Page<UserInfo> getUserInfoPage(Pageable pageable) throws InvalidDataException {
        try {
            Page<UserAccount> userAccounts = userAccountRepository.findAll(pageable);
            Long totalPages = userAccounts.getTotalElements();

            List<Long> userAccountIdList = userAccounts.stream()
                    .map(UserAccount::getUserAccountId)
                    .collect(Collectors.toList());
            List<UserAccountUserLink> userAccountUserLinkList = userAccountUserLinkRepository.
                    findAllByUserAccountIdIn(userAccountIdList);

            List<Long> userIdList = userAccountUserLinkList.stream()
                    .map(UserAccountUserLink::getUserId)
                    .distinct()
                    .collect(Collectors.toList());
            List<User> userList = userRepository.findAllByIdIn(userIdList);

            List<UserAccountRoleLink> userAccountRoleLinkList = userAccountRoleLinkRepository.
                    findAllByUserAccountIdIn(userAccountIdList);

            List<Long> roleIdList = userAccountRoleLinkList.stream()
                    .map(UserAccountRoleLink::getRoleId)
                    .distinct()
                    .collect(Collectors.toList());
            List<Role> roleList = roleRepository.findAllByRoleIdIn(roleIdList);

            List<UserInfo> userInfoList = userInfoMapper.buildUserInfoList(userList, userAccounts.getContent(),
                    userAccountUserLinkList, roleList, userAccountRoleLinkList);

            return new PageImpl<>(userInfoList, pageable, totalPages);
        } catch (Exception e) {
            LOGGER.error("Can not build userInfo page", e);
            throw new InvalidDataException("Something Wrong", 0, (Throwable) e);
        }
    }

    @Override
    public Page<UserInfo> getUserInfoPage(Pageable pageable, String roleCode) {
        try {
            Role role = roleRepository.findByRoleCode(roleCode);

            List<UserAccountRoleLink> userAccountRoleLinkList = userAccountRoleLinkRepository.findAllByRoleId(role.getRoleId());

            List<Long> userAccountIdList = userAccountRoleLinkList.stream()
                    .map(UserAccountRoleLink::getUserAccountId)
                    .collect(Collectors.toList());

            Page<UserAccount> userAccounts = userAccountRepository.findUserAccountByUserAccountIdIn(pageable, userAccountIdList);
            Long totalPages = userAccounts.getTotalElements();

            List<UserAccountUserLink> userAccountUserLinkList = userAccountUserLinkRepository.
                    findAllByUserAccountIdIn(userAccountIdList);

            List<Long> userIdList = userAccountUserLinkList.stream()
                    .map(UserAccountUserLink::getUserId)
                    .collect(Collectors.toList());

            List<User> userList = userRepository.findAllByIdIn(userIdList);

            List<UserInfo> userInfoList = userInfoMapper.buildUserInfoList(userList, userAccounts.getContent(),
                    userAccountUserLinkList, Collections.singletonList(role), userAccountRoleLinkList);

            return new PageImpl<>(userInfoList, pageable, totalPages);
        } catch (Exception e) {
            LOGGER.error("Can not build userInfo page", e);
            throw new InvalidDataException("Something Wrong", 0, (Throwable) e);
        }
    }

    @Override
    public Page<UserInfo> getUserInfoPage(Pageable pageable, String roleCode, String fullName) {
        try {
            Role role = roleRepository.findByRoleCode(roleCode);

            List<UserAccountRoleLink> userAccountRoleLinkList = userAccountRoleLinkRepository.findAllByRoleId(role.getRoleId());

            List<Long> userAccountIdList = userAccountRoleLinkList.stream()
                    .map(UserAccountRoleLink::getUserAccountId)
                    .collect(Collectors.toList());

            Page<UserAccount> userAccounts = userAccountRepository.findUserAccountByUserAccountIdIn(pageable, userAccountIdList);
            List<UserAccount> userAccountList = userAccounts.getContent().stream()
                    .filter(a -> a.getUserAccountName().contains(fullName))
                    .collect(Collectors.toList());
            Long totalPages = (long) userAccountList.size();

            userAccountIdList = userAccountList.stream()
                    .map(UserAccount::getUserAccountId)
                    .collect(Collectors.toList());

            List<UserAccountUserLink> userAccountUserLinkList = userAccountUserLinkRepository.
                    findAllByUserAccountIdIn(userAccountIdList);

            List<Long> userIdList = userAccountUserLinkList.stream()
                    .map(UserAccountUserLink::getUserId)
                    .collect(Collectors.toList());

            List<User> userList = userRepository.findAllByIdIn(userIdList);

            List<UserInfo> userInfoList = userInfoMapper.buildUserInfoList(userList, userAccounts.getContent(),
                    userAccountUserLinkList, Collections.singletonList(role), userAccountRoleLinkList);

            return new PageImpl<>(userInfoList, pageable, totalPages);
        } catch (Exception e) {
            LOGGER.error("Can not build userInfo page", e);
            throw new InvalidDataException("Something Wrong", 0, (Throwable) e);
        }
    }

    @Override
    public Boolean isUserExist(String login) {
        return userRepository.existsByUserLogin(login);
    }

    @Override
    public UserInfo getUserInfoByEmail(String email) {
        try {
            UserAccount userAccount = userAccountRepository.findUserAccountsByUserAccountEmail(email);
            if (userAccount == null) {
                LOGGER.error("Find by email: user account not found");
                throw new InvalidDataException("Something Wrong", 0);
            }
            return getUserInfoByUserAccountId(userAccount.getUserAccountId());
        } catch (Exception e) {
            LOGGER.error("Find by email: user account not found", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }
    }
}
