package com.dvkuksa.services.impl;

import com.dvkuksa.model.dal.entity.MedicalReport;
import com.dvkuksa.model.dal.entity.UserAccount;
import com.dvkuksa.model.dal.repository.MedicalReportRepository;
import com.dvkuksa.model.dal.repository.UserAccountRepository;
import com.dvkuksa.model.exception.InvalidDataException;
import com.dvkuksa.services.MedicalReportService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class MedicalReportServiceImpl implements MedicalReportService {

    private static final Logger LOGGER = LogManager.getLogger(MedicalReportServiceImpl.class);

    private final MedicalReportRepository medicalReportRepository;
    private final UserAccountRepository userAccountRepository;

    public MedicalReportServiceImpl(MedicalReportRepository medicalReportRepository,
                                    UserAccountRepository userAccountRepository) {
        this.medicalReportRepository = medicalReportRepository;
        this.userAccountRepository = userAccountRepository;
    }

    @Override
    public Page<MedicalReport> findAll(Pageable page) {
        return medicalReportRepository.findAll(page);
    }

    @Override
    public MedicalReport findById(Long medicalReportId) {
        return medicalReportRepository.findMedicalReportById(medicalReportId);
    }

    @Override
    public Page<MedicalReport> findDoctorMedicalReportPage(Pageable page, Long doctorId) {
        return medicalReportRepository.findAllByDoctor(page, userAccountRepository.findUserAccountByUserAccountId(doctorId));
    }

    @Override
    public Page<MedicalReport> findPatientMedicalReportPage(Pageable page, Long patientId) {
        return medicalReportRepository.findAllByPatient(page, userAccountRepository.findUserAccountByUserAccountId(patientId));
    }

    @Override
    public List<MedicalReport> findPatientMedicalReportList(Long patientId) {
        return medicalReportRepository.findAllByPatientUserAccountId(patientId);
    }

    @Override
    public MedicalReport save(MedicalReport medicalReport, Long doctorId, Long patientId) {
        try {
            UserAccount patient = userAccountRepository.findUserAccountByUserAccountId(patientId);
            UserAccount doctor = userAccountRepository.findUserAccountByUserAccountId(doctorId);
            medicalReport.setDoctor(doctor);
            medicalReport.setPatient(patient);
            medicalReport.setCreationDate(LocalDate.now());
            return medicalReportRepository.save(medicalReport);
        } catch (Exception e) {
            LOGGER.error("Medical report save error", e);
            throw new InvalidDataException("Something Wrong", 0, e);
        }
    }
}
