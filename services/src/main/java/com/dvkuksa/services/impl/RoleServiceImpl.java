package com.dvkuksa.services.impl;

import com.dvkuksa.model.dal.entity.Role;
import com.dvkuksa.model.dal.repository.RoleRepository;
import com.dvkuksa.services.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }
}
