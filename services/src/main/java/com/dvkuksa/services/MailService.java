package com.dvkuksa.services;

import org.springframework.stereotype.Service;

@Service
public interface MailService {

    void send(String emailTo, String subject, String message);
}
