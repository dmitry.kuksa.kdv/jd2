package com.dvkuksa.front.controller;

import com.dvkuksa.model.utils.UiMessageHelper;
import com.dvkuksa.model.dto.ReCaptchaResponse;
import com.dvkuksa.model.dto.userinfo.UserInfo;
import com.dvkuksa.model.exception.InvalidDataException;
import com.dvkuksa.services.AdministrationService;
import com.dvkuksa.services.UserInfoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

@Controller
public class RegistrationController {

    @Value("${recaptcha.verify.url}")
    private String captchaUrl;
    @Value("${recaptcha.secret}")
    private String secret;
    private final static String CAPTCHA_PARAMS = "?secret=%s&response=%s";
    private static final String PATIENT_ROLE_CODE = "patient";

    private final AdministrationService administrationService;
    private final UserInfoService userInfoService;
    private final RestTemplate restTemplate;


    public RegistrationController(AdministrationService administrationService,
                                  UserInfoService userInfoService,
                                  RestTemplate restTemplate) {
        this.administrationService = administrationService;
        this.userInfoService = userInfoService;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String save(@Valid UserInfo userInfo,
                       BindingResult bindingResult,
                       @RequestParam("g-recaptcha-response") String captchaResponse,
                       String passwordConfirm,
                       Model model) {
        try {
            String url = String.format(captchaUrl + CAPTCHA_PARAMS, secret, captchaResponse);
            ReCaptchaResponse response = restTemplate.postForObject(url, Collections.emptyList(), ReCaptchaResponse.class);

            if (response == null || !response.getSuccess()) {
                model.addAttribute("captchaError", "Fill captcha");
            }

            if (userInfo.getPassword() != null && !userInfo.getPassword().equals(passwordConfirm)) {
                model.addAttribute("passwordError", "Passwords are different!");
            }

            if (bindingResult.hasErrors() || !response.getSuccess() || model.containsAttribute("passwordError")) {
                Map<String, String> errors = UiMessageHelper.getErrors(bindingResult);
                model.mergeAttributes(errors);
                return "registration";
            }

            if (userInfoService.isUserExist(userInfo.getUserLogin())) {
                model.addAttribute("userLoginError", "User already exist");
                return "registration";
            }

            administrationService.registration(userInfo, PATIENT_ROLE_CODE, null);
        } catch (Exception e) {
            UiMessageHelper.addErrorAlertMessage(model, e.getMessage());
            return "registration";
        }
        UiMessageHelper.addSuccessAlertMessage(model, "Success. Check your email");
        return "login";
    }


    @GetMapping("/activate/{code}")
    public String activate(Model model, @PathVariable String code) {
        try {
            Boolean isActivated = administrationService.activateUser(code);
            if (isActivated) {
                UiMessageHelper.addSuccessAlertMessage(model, "User successfully activated");
            } else {
                UiMessageHelper.addErrorAlertMessage(model, "Activation code is not found!");
            }
        } catch (InvalidDataException e) {
            UiMessageHelper.addErrorAlertMessage(model, e.getMessage());
            return "login";
        }
        return "login";
    }

    @GetMapping("/passwordReset")
    public String resetPassword(Model model) {
        return "passwordReset";
    }

    @PostMapping("/passwordReset")
    public String resetPassword(Model model, @RequestParam String email) {
        try {
            UserInfo userInfo = userInfoService.getUserInfoByEmail(email);
            administrationService.resetUserPassword(userInfo.getUserId(), userInfo.getUserAccountEmail());
            UiMessageHelper.addSuccessAlertMessage(model, "Success. Check your email");
        } catch (InvalidDataException e) {
            UiMessageHelper.addErrorAlertMessage(model, e.getMessage());
            return "passwordReset";
        }
        return "login";
    }

    @GetMapping("/passwordUpdate/{code}")
    public String updatePassword(Model model, @PathVariable String code) {
        try {
            if (administrationService.validateActivateCode(code)) {
                model.addAttribute("code", code);
            }
        } catch (InvalidDataException e) {
            UiMessageHelper.addErrorAlertMessage(model, e.getMessage());
            return "login";
        }
        return "passwordUpdate";
    }

    @PostMapping("/passwordUpdate/{code}")
    public String updatePassword(@PathVariable String code,
                                 @RequestParam String password,
                                 @RequestParam String passwordConfirm,
                                 Model model) {
        if (password != null && !password.equals(passwordConfirm)) {
            model.addAttribute("passwordError", "Passwords are different!");
            return "passwordUpdate";
        }
        try {
            administrationService.updateUserPassword(code, password);
            administrationService.activateUser(code);
            UiMessageHelper.addSuccessAlertMessage(model, "Success. Password was change");
        } catch (InvalidDataException e) {
            UiMessageHelper.addErrorAlertMessage(model, e.getMessage());
            return "passwordReset";
        }
        return "login";
    }
}
