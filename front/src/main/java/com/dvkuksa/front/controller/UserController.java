package com.dvkuksa.front.controller;

import com.dvkuksa.model.utils.UiMessageHelper;
import com.dvkuksa.model.dto.userinfo.UserInfo;
import com.dvkuksa.services.AppointmentService;
import com.dvkuksa.services.RoleService;
import com.dvkuksa.services.AdministrationService;
import com.dvkuksa.services.UserInfoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/user")
public class UserController {

    private static final String PATIENT_ROLE_CODE = "patient";

    @Value("${upload.path}")
    private String uploadPath;

    private final UserInfoService userInfoService;
    private final AdministrationService administrationService;
    private final RoleService roleService;
    private final AppointmentService appointmentService;

    public UserController(UserInfoService userInfoService,
                          AdministrationService administrationService,
                          RoleService roleService,
                          AppointmentService appointmentService) {
        this.userInfoService = userInfoService;
        this.administrationService = administrationService;
        this.roleService = roleService;
        this.appointmentService = appointmentService;
    }

    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    @GetMapping
    public String findAll(Pageable pageable,
                          Model model,
                          @RequestParam(required = false) String roleCode,
                          @RequestParam(required = false) String fullName) {
        if (!StringUtils.isEmpty(fullName)) {
            model.addAttribute("page", userInfoService.getUserInfoPage(pageable, roleCode, fullName));
            model.addAttribute("fullName", fullName);
        } else if (!StringUtils.isEmpty(roleCode)) {
            model.addAttribute("page", userInfoService.getUserInfoPage(pageable, roleCode));
        } else {
            model.addAttribute("page", userInfoService.getUserInfoPage(pageable));
        }
        model.addAttribute("roleCode", roleCode);
        model.addAttribute("url", "/user");
        model.addAttribute("roleList", roleService.getAllRoles());
        return "usersPage";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    @GetMapping("{userAccountId}")
    public String find(@PathVariable Long userAccountId, Model model) {
        model.addAttribute("userInfo", userInfoService.getUserInfoByUserAccountId(userAccountId));
        model.addAttribute("roleList", roleService.getAllRoles());
        return "userEditPage";
    }

    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    @GetMapping("newUser")
    public String add(Model model) {
        model.addAttribute("roleList", roleService.getAllRoles());
        return "userAddPage";
    }

    @PreAuthorize("hasAuthority('admin')")
    @PostMapping
    public String save(@Valid UserInfo userInfo,
                       BindingResult bindingResult,
                       @RequestParam(required = false) String passwordConfirm,
                       String roleCode,
                       @RequestParam(name = "file", required = false) MultipartFile file,
                       Model model) {
        try {
            model.addAttribute("roleList", roleService.getAllRoles());
            model.addAttribute("userInfo", userInfo);
            model.addAttribute("roleCode", roleCode);

            if (bindingResult.hasErrors()) {
                model.mergeAttributes(UiMessageHelper.getErrors(bindingResult));
                if (userInfo.getUserAccountId() != null) {
                    model.addAttribute("error", true);
                    model.addAttribute("userInfo", userInfoService.getUserInfoByUserAccountId(userInfo.getUserAccountId()));
                    return "userEditPage";
                } else {
                    return "userAddPage";
                }
            }

            if (userInfo.getUserAccountId() != null) {
                administrationService.update(userInfo, roleCode, file);
                return "redirect:/user/" + userInfo.getUserAccountId();
            } else {
                if (userInfo.getPassword() == null || !userInfo.getPassword().equals(passwordConfirm)) {
                    model.addAttribute("passwordError", "Passwords are different!");
                    return "userAddPage";
                }
                administrationService.registration(userInfo, roleCode, file);
                return "redirect:/user";
            }
        } catch (Exception e) {
            UiMessageHelper.addErrorAlertMessage(model, e.getMessage());
            return userInfo.getUserAccountId() != null ? "userEditPage" : "userAddPage";
        }
    }

    @PreAuthorize("hasAnyAuthority('admin', 'manager')")
    @PostMapping("{userAccountId}/resetPassword")
    public String resetPassword(@RequestParam String newPassword,
                                @RequestParam String passwordConfirm,
                                @RequestParam Long userId,
                                @PathVariable Long userAccountId,
                                Model model) {
        try {
            if (newPassword != null && !newPassword.equals(passwordConfirm)) {
                model.addAttribute("resetPasswordError", true);
                model.addAttribute("passwordError", "Passwords are different!");
                model.addAttribute("userInfo", userInfoService.getUserInfoByUserAccountId(userAccountId));
                model.addAttribute("roleList", roleService.getAllRoles());
                return "userEditPage";
            }
            administrationService.updateUserPassword(userId, newPassword);
            return "redirect:/user/" + userAccountId;
        } catch (Exception e) {
            UiMessageHelper.addErrorAlertMessage(model, e.getMessage());
            return "userEditPage";
        }
    }

    @GetMapping("profile")
    public String findProfile(@AuthenticationPrincipal UserInfo userInfo,
                              @RequestParam(required = false) String date,
                              Model model) { //todo: add enum for role codes
        model.addAttribute("userInfo", userInfo);

        if ("patient".equals(userInfo.getUserAccountRole().getRoleCode())) {
            model.addAttribute("userAppointmentList", appointmentService.findPatientAppointmentList(userInfo.getUserAccountId()));
        }

        if ("doctor".equals(userInfo.getUserAccountRole().getRoleCode())) {
            model.addAttribute("date", date);
            model.addAttribute("currentDate", LocalDate.now());
            LocalDate localDate = StringUtils.isEmpty(date) ? LocalDate.now() : LocalDate.parse(date);
            model.addAttribute("appointmentList", appointmentService.findDoctorAppointmentListByDate(localDate, userInfo.getUserAccountId()));
        }
        return "profile";
    }

    @PostMapping("profile/changePassword")
    public String changePassword(@AuthenticationPrincipal UserInfo userInfo,
                                 @RequestParam String currentPassword,
                                 @RequestParam String newPassword,
                                 @RequestParam String passwordConfirm,
                                 @RequestParam Long userId,
                                 @RequestParam Long userAccountId,
                                 Model model) {
        try {
            model.addAttribute("userInfo", userInfo);
            if ("patient".equals(userInfo.getUserAccountRole().getRoleCode())) {
                model.addAttribute("userAppointmentList",
                        appointmentService.findPatientAppointmentList(userInfo.getUserAccountId()));
            }
            if ("doctor".equals(userInfo.getUserAccountRole().getRoleCode())) {
                model.addAttribute("currentDate", LocalDate.now());
                model.addAttribute("appointmentList",
                        appointmentService.findDoctorAppointmentListByDate(LocalDate.now(), userInfo.getUserAccountId()));
            }

            if (newPassword != null && !newPassword.equals(passwordConfirm)) {
                model.addAttribute("resetPasswordError", true);
                model.addAttribute("passwordError", "Passwords are different!");
                return "profile";
            }
            if (!userInfo.getUserAccountId().equals(userAccountId)) {
                model.addAttribute("message", "Action forbidden!");
                return "profile";
            }
            if (administrationService.validatePassword(userId, currentPassword)) {
                administrationService.updateUserPassword(userId, newPassword);
                UiMessageHelper.addSuccessAlertMessage(model, "Success. Password was change");
            } else {
                model.addAttribute("resetPasswordError", true);
                UiMessageHelper.addErrorAlertMessage(model, "Password verification error");
            }
            return "profile";
        } catch (Exception e) {
            UiMessageHelper.addErrorAlertMessage(model, e.getMessage());
            return "profile";
        }
    }
}