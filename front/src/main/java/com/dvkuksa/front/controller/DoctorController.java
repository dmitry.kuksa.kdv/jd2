package com.dvkuksa.front.controller;

import com.dvkuksa.model.utils.DateHelper;
import com.dvkuksa.model.dto.appointmentinfo.AppointmentInfo;
import com.dvkuksa.model.dto.userinfo.UserInfo;
import com.dvkuksa.services.AppointmentService;
import com.dvkuksa.services.UserInfoService;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/doctor")
public class DoctorController {

    private static final String DOCTOR_ROLE_CODE = "doctor";
    private static final List<String> TIME_LIST = Arrays.asList( //todo: для хранения создать entity TimetableTemplate
            "08:00", "08:20", "08:40",
            "09:00", "09:20", "09:40",
            "10:00", "10:20", "10:40",
            "11:00", "11:20", "11:40",
            "12:00", "12:20", "12:40",
            "13:00", "13:20", "13:40",
            "14:00", "14:20", "14:40",
            "15:00", "15:20", "15:40",
            "16:00", "16:20", "16:40",
            "17:00", "17:20", "17:40",
            "18:00", "18:20", "18:40",
            "19:00", "19:20", "19:40",
            "20:00", "20:20", "20:40",
            "21:00", "21:20", "21:40");

    private final UserInfoService userInfoService;
    private final AppointmentService appointmentService;

    public DoctorController(UserInfoService userInfoService, AppointmentService appointmentService) {
        this.userInfoService = userInfoService;
        this.appointmentService = appointmentService;
    }

    @GetMapping
    public String findAll(Pageable pageable, Model model) {
        model.addAttribute("page", userInfoService.getUserInfoPage(pageable, DOCTOR_ROLE_CODE));
        model.addAttribute("url", "/doctor");
        return "doctor";
    }

    @GetMapping("{doctorId}")
    public String find(@AuthenticationPrincipal UserInfo currentUserInfo,
                       @PathVariable Long doctorId,
                       @RequestParam(required = false) String date,
                       Model model) {
        model.addAttribute("userInfo", userInfoService.getUserInfoByUserAccountId(doctorId));
        model.addAttribute("timeList", TIME_LIST);
        model.addAttribute("date", date);
        model.addAttribute("currentDate", LocalDate.now());

        LocalDate localDate = StringUtils.isEmpty(date) ? LocalDate.now() : LocalDate.parse(date);
        List<AppointmentInfo> appointmentInfoList = appointmentService.findDoctorAppointmentListByDate(localDate, doctorId);
        model.addAttribute("appointmentList", appointmentInfoList);

        model.addAttribute("appointmentListForTable", appointmentInfoList.stream()
                .filter(a -> !a.getIsRegistered()).collect(Collectors.toList()));

        model.addAttribute("url", "/doctor");
        return "doctorProfile";
    }

    @PostMapping("timetable")
    public String saveTimeTable(@RequestParam Map<String, String> model,
                                @RequestParam String date,
                                @RequestParam Long doctorId) {
        List<String> dateTimeList = model.entrySet().stream()
                .filter(m -> m.getKey().startsWith("time::"))
                .map(m -> date.concat(" " + m.getValue()))
                .collect(Collectors.toList());

        List<LocalDateTime> localDateTimeList = DateHelper.createLocalDateList(dateTimeList);
        appointmentService.saveAll(doctorId, localDateTimeList);

        return "redirect:/doctor/" + doctorId;
    }
}
