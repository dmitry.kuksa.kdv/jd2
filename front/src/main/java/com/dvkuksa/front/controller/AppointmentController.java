package com.dvkuksa.front.controller;

import com.dvkuksa.model.dto.userinfo.UserInfo;
import com.dvkuksa.services.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

@Controller
@RequestMapping("/appointment")
public class AppointmentController {

    private final AppointmentService appointmentService;

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @GetMapping
    public String find(@RequestParam(required = false) String date,
                       Model model) {
        model.addAttribute("date", date);
        LocalDate localDate = StringUtils.isEmpty(date) ? LocalDate.now() : LocalDate.parse(date);
        model.addAttribute("appointmentList", appointmentService.findAppointmentInfoListByDate(localDate));
        return "appointment";
    }

    @PostMapping
    public String bookAnAppointment(@AuthenticationPrincipal UserInfo currentUserInfo,
                                    @RequestParam Long appointmentId,
                                    @RequestParam Long patientId) {
        appointmentService.bookAnAppointment(appointmentId, patientId);

        if (currentUserInfo.getUserAccountRole().getRoleCode().equals("manager")) {
            return "redirect:/appointment";
        } else {
            return "redirect:/user/profile";
        }
    }
}
