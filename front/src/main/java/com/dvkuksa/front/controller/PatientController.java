package com.dvkuksa.front.controller;

import com.dvkuksa.model.utils.DateHelper;
import com.dvkuksa.model.dto.userinfo.UserInfo;
import com.dvkuksa.services.AppointmentService;
import com.dvkuksa.services.MedicalReportService;
import com.dvkuksa.services.UserInfoService;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

@Controller
@RequestMapping("/patient")
public class PatientController {
    private static final String PATIENT_ROLE_CODE = "patient";

    private final UserInfoService userInfoService;
    private final AppointmentService appointmentService;
    private final MedicalReportService medicalReportService;

    public PatientController(UserInfoService userInfoService,
                             AppointmentService appointmentService,
                             MedicalReportService medicalReportService) {
        this.userInfoService = userInfoService;
        this.appointmentService = appointmentService;
        this.medicalReportService = medicalReportService;
    }


    @PreAuthorize("hasAnyAuthority('doctor', 'admin', 'manager')")
    @GetMapping
    public String findAll(Pageable pageable, Model model) {
        model.addAttribute("page", userInfoService.getUserInfoPage(pageable, PATIENT_ROLE_CODE));
        model.addAttribute("url", "/patient");
        model.addAttribute("url", "/patient");
        return "patient";
    }


    @PreAuthorize("hasAnyAuthority('doctor', 'admin', 'manager')")
    @GetMapping("{patientId}")
    public String find(@AuthenticationPrincipal UserInfo currentUserInfo,
                       @PathVariable Long patientId,
                       @RequestParam(required = false) String date,
                       Model model) {
        UserInfo patientInfo = userInfoService.getUserInfoByUserAccountId(patientId);
        model.addAttribute("userInfo", patientInfo);
        model.addAttribute("date", date);

        LocalDate localDate = StringUtils.isEmpty(date) ? LocalDate.now() : LocalDate.parse(date);
        model.addAttribute("userAppointmentList", appointmentService.findPatientAppointmentList(patientId));
        model.addAttribute("userMedicalReportList", medicalReportService.findPatientMedicalReportList(patientId));
        return "patientProfile";
    }


    @GetMapping("{patientId}/addReport")
    public String add(@AuthenticationPrincipal UserInfo currentUserInfo,
                      @PathVariable Long patientId,
                      Model model) {
        model.addAttribute("patient", userInfoService.getUserInfoByUserAccountId(patientId));
        model.addAttribute("currentDate", DateHelper.getFormattedDate(LocalDate.now()));
        return "reportAddPage";
    }
}
