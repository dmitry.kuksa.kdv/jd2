package com.dvkuksa.front.controller;

import com.dvkuksa.model.dal.entity.MedicalReport;
import com.dvkuksa.model.dto.userinfo.UserInfo;
import com.dvkuksa.services.MedicalReportService;
import com.dvkuksa.services.UserInfoService;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/report")
public class MedicalReportController {

    private final MedicalReportService medicalReportService;
    private final UserInfoService userInfoService;

    public MedicalReportController(MedicalReportService medicalReportService, UserInfoService userInfoService) {
        this.medicalReportService = medicalReportService;
        this.userInfoService = userInfoService;
    }

    @PreAuthorize("hasAuthority('doctor')")
    @GetMapping
    public String findAll(@AuthenticationPrincipal UserInfo currentUserInfo,
                          Model model,
                          Pageable pageable,
                          @RequestParam(required = false) Long patientId,
                          @RequestParam(required = false) Long doctorId) {
        model.addAttribute("page", medicalReportService.findAll(pageable));
        model.addAttribute("url", "/report");
        return "reportPage";
    }

    @PreAuthorize("hasAuthority('doctor')")
    @GetMapping("{reportId}")
    public String find(@PathVariable Long reportId, Model model) {
        model.addAttribute("medicalReport", medicalReportService.findById(reportId));
        return "reportViewPage";
    }

    @PreAuthorize("hasAuthority('doctor')")
    @PostMapping
    public String save(@AuthenticationPrincipal UserInfo currentUserInfo,
                      @RequestParam Long patientId,
                      MedicalReport medicalReport,
                      Model model) {
        medicalReportService.save(medicalReport, currentUserInfo.getUserAccountId(), patientId);
        return "redirect:/patient/" + patientId;
    }
}
