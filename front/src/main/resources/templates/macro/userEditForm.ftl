<#macro userEditForm path isUserExist>
    <form action="${path}" method="post" enctype="multipart/form-data">
        <div class="form-row">
            <!-- Role -->
            <div class="form-group col-md-4">
                <label>Role</label>
                <select name="roleCode" class="form-control">
                    <#list roleList as role>
                        <option value="${role.roleCode}" name="roleCode"
                                <#if isUserExist>
                                <#if userInfo?? && userInfo.userAccountRole.roleCode == role.roleCode>selected="selected"</#if>>${role.roleName}
                            <#else>
                                <#if roleCode?? && roleCode == role.roleCode>selected="selected"</#if>>${role.roleName}
                            </#if>
                        </option>
                    </#list>
                </select>
            </div>
        </div>
        <div class="form-row">
            <!-- Photo -->
            <div class="form-group col-md-4">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="file">
                    <label class="custom-file-label" for="customFile">Choose photo</label>
                </div>
            </div>
            <script>
                $('#customFile').on('change',function(){
                    //get the file name
                    var fileName = $(this).val();
                    //replace the "Choose a file" label
                    $(this).next('.custom-file-label').html(fileName);
                })
            </script>
        </div>
        <div class="form-row">
            <!-- Surname -->
            <div class="form-group col-md-4">
                <label>Surname</label>
                <input type="text" class="form-control ${(lastNameError??)?string('is-invalid', '')}"
                       value="<#if userInfo??>${userInfo.lastName}</#if>" name="lastName"
                       placeholder="User surname"/>
                <#if lastNameError??>
                    <div class="invalid-feedback">
                        ${lastNameError}
                    </div>
                </#if>
            </div>
            <!-- Name -->
            <div class="form-group col-md-4">
                <label>Name</label>
                <input type="text" class="form-control ${(firstNameError??)?string('is-invalid', '')}"
                       value="<#if userInfo??>${userInfo.firstName}</#if>" name="firstName"
                       placeholder="User name"/>
                <#if firstNameError??>
                    <div class="invalid-feedback">
                        ${firstNameError}
                    </div>
                </#if>
            </div>
            <!-- Middle name -->
            <div class="form-group col-md-4">
                <label>Middle name</label>
                <input type="text" class="form-control ${(middleNameError??)?string('is-invalid', '')}"
                       value="<#if userInfo??>${userInfo.middleName}</#if>" name="middleName"
                       placeholder="User middle name"/>
                <#if middleNameError??>
                    <div class="invalid-feedback">
                        ${middleNameError}
                    </div>
                </#if>
            </div>
        </div>
        <#if !isUserExist>
            <div class="form-row">
                <!-- Login -->
                <div class="form-group col-md-4">
                    <label for="login">Login</label>
                    <input type="text" name="userLogin" id="login" value="<#if userInfo??>${userInfo.userLogin}</#if>"
                           class="form-control ${(userLoginError??)?string('is-invalid', '')}"
                           placeholder="User login"/>
                    <#if userLoginError??>
                        <div class="invalid-feedback">
                            ${userLoginError}
                        </div>
                    </#if>
                </div>
                <!-- Password -->
                <div class=" form-group col-md-4">
                    <label>Password</label>
                    <input type="password" name="password"
                           class="form-control ${(passwordError??)?string('is-invalid', '')}"
                           placeholder="Password" required/>
                    <#if passwordError??>
                        <div class="invalid-feedback">
                            ${passwordError}
                        </div>
                    </#if>
                </div>
                <!-- Password confirm -->
                <div class="form-group col-md-4">
                    <label>Password confirm</label>
                    <input type="password" name="passwordConfirm"
                           class="form-control ${(passwordError??)?string('is-invalid', '')}"
                           placeholder="Password confirm" required/>
                    <#if passwordError??>
                        <div class="invalid-feedback">
                            ${passwordError}
                        </div>
                    </#if>
                </div>
            </div>
        <#else>
            <input type="hidden" name="userLogin" value="<#if userInfo??>${userInfo.userLogin}</#if>"/>
        </#if>
        <div class="form-row">
            <!-- Email -->
            <div class="form-group col-md-4">
                <label>Email</label>
                <input type="email" class="form-control ${(userAccountEmailError??)?string('is-invalid', '')}"
                       value="<#if userInfo??>${userInfo.userAccountEmail}</#if>" name="userAccountEmail"
                       placeholder="temp@gmail.com"/>
                <#if userAccountEmailError??>
                    <div class="invalid-feedback">
                        ${userAccountEmailError}
                    </div>
                </#if>
            </div>
            <!-- Phone number -->
            <div class="form-group col-md-4">
                <label>Phone number</label>
                <input type="text" class="form-control ${(userAccountPhoneNumberError??)?string('is-invalid', '')}"
                       value="<#if userInfo??>${userInfo.userAccountPhoneNumber}</#if>" name="userAccountPhoneNumber"
                       placeholder="+375"/>
                <#if userAccountPhoneNumberError??>
                    <div class="invalid-feedback">
                        ${userAccountPhoneNumberError}
                    </div>
                </#if>
            </div>
        </div>
        <!-- Enabled -->
        <div class="form-row">
            <div class="form-group col-md-6">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="isUserEnabled" class="custom-control-input" id="isUserEnabled"
                           <#if userInfo?? && userInfo.isUserEnabled?? && userInfo.isUserEnabled>checked</#if>>
                    <label class="custom-control-label" for="isUserEnabled">Enabled</label>
                </div>
            </div>
        </div>

        <#if isUserExist>
            <input type="hidden" name="userAccountId" value="<#if userInfo??>${userInfo.userAccountId}</#if>"/>
            <input type="hidden" name="userId" value="<#if userInfo??>${userInfo.userId}</#if>"/>
        </#if>
        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <button class="btn btn-outline-info btn-block mt-4" type="submit">
            <#if isUserExist>Update<#else>Add</#if>
        </button>
    </form>

</#macro>

<#macro resetPassword path isAdmin>

    <form action="${path}" method="post">
        <div class="form-row">
            <#if !isAdmin>
                <!-- Current password -->
                <div class=" form-group col-md-4">
                    <label>Current password</label>
                    <input type="password" name="currentPassword"
                           class="form-control ${(currentPasswordError??)?string('is-invalid', '')}"
                           placeholder="Current password" required/>
                    <#if currentPasswordError??>
                        <div class="invalid-feedback">
                            ${currentPasswordError}
                        </div>
                    </#if>
                </div>
            </#if>
            <!-- New password -->
            <div class=" form-group col-md-4">
                <label>New password</label>
                <input type="password" name="newPassword"
                       class="form-control ${(passwordError??)?string('is-invalid', '')}"
                       placeholder="New password" required/>
                <#if passwordError??>
                    <div class="invalid-feedback">
                        ${passwordError}
                    </div>
                </#if>
            </div>
            <!-- Password confirm -->
            <div class="form-group col-md-4">
                <label>Password confirm</label>
                <input type="password" name="passwordConfirm"
                       class="form-control ${(passwordError??)?string('is-invalid', '')}"
                       placeholder="Password confirm" required/>
                <#if passwordError??>
                    <div class="invalid-feedback">
                        ${passwordError}
                    </div>
                </#if>
            </div>
            <input type="hidden" name="userAccountId" value="<#if userInfo??>${userInfo.userAccountId}</#if>"/>
            <input type="hidden" name="userId" value="<#if userInfo??>${userInfo.userId}</#if>"/>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <button class="btn btn-outline-info btn-block mt-4" type="submit">Save</button>
        </div>
    </form>
</#macro>

<#macro changePassword path>

    <form class="text-center border border-light p-5" action="${path}" method="post">
        <div class="row justify-content-md-center">

            <div class="col-sm-7">
                <h4 class="text-lg-center mx-4">Change password</h4>
                <hr>
                <!-- Current password -->
                <input type="password" name="password"
                       class="form-control mb-4 ${(currentPasswordError??)?string('is-invalid', '')}"
                       placeholder="Current password" required/>
                <#if currentPasswordError??>
                    <div class="invalid-feedback">
                        ${currentPasswordError}
                    </div>
                </#if>

                <!-- New password -->

                <input type="password" name="password"
                       class="form-control mb-4 ${(passwordError??)?string('is-invalid', '')}"
                       placeholder="New password" required/>
                <#if passwordError??>
                    <div class="invalid-feedback">
                        ${passwordError}
                    </div>
                </#if>
                <!-- Password confirm -->
                <input type="password" name="passwordConfirm"
                       class="form-control mb-4  ${(passwordError??)?string('is-invalid', '')}"
                       placeholder="Password confirm" required/>
                <#if passwordError??>
                    <div class="invalid-feedback">
                        ${passwordError}
                    </div>
                </#if>
                <hr>
                <input type="hidden" name="userAccountId" value="<#if userInfo??>${userInfo.userAccountId}</#if>"/>
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <button class="btn btn-outline-info btn-block mt-4" type="submit">Save</button>
            </div>
        </div>
    </form>

</#macro>