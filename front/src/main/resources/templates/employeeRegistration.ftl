<#import "macro/common.ftl" as c>
<#import "macro/login.ftl" as l>
<@c.page>
    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>Registration</strong>
    </h5>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <form action="/employee" method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Login:</label>
            <div class="col-sm-6">
                <input type="text" name="userLogin" value="<#if userInfo??>${userInfo.userLogin}</#if>"
                       class="form-control ${(userLoginError??)?string('is-invalid', '')}"
                       placeholder="User login"/>
                <#if userLoginError??>
                    <div class="invalid-feedback">
                        ${userLoginError}
                    </div>
                </#if>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Password:</label>
            <div class="col-sm-6">
                <input type="password" name="password"
                       class="form-control ${(passwordError??)?string('is-invalid', '')}"
                       placeholder="Password"/>
                <#if passwordError??>
                    <div class="invalid-feedback">
                        ${passwordError}
                    </div>
                </#if>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Password confirm:</label>
            <div class="col-sm-6">
                <input type="password" name="passwordConfirm"
                       class="form-control ${(passwordError??)?string('is-invalid', '')}"
                       placeholder="Password confirm"/>
                <#if passwordError??>
                    <div class="invalid-feedback">
                        ${passwordError}
                    </div>
                </#if>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Email:</label>
            <div class="col-sm-6">
                <input type="email" class="form-control ${(userAccountEmailError??)?string('is-invalid', '')}"
                       value="<#if userInfo??>${userInfo.userAccountEmail}</#if>" name="userAccountEmail"
                       placeholder="temp@gmail.com"/>
                <#if userAccountEmailError??>
                    <div class="invalid-feedback">
                        ${userAccountEmailError}
                    </div>
                </#if>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Surname:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control ${(lastNameError??)?string('is-invalid', '')}"
                       value="<#if userInfo??>${userInfo.lastName}</#if>" name="lastName"
                       placeholder="User surname"/>
                <#if lastNameError??>
                    <div class="invalid-feedback">
                        ${lastNameError}
                    </div>
                </#if>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control ${(firstNameError??)?string('is-invalid', '')}"
                       value="<#if userInfo??>${userInfo.firstName}</#if>" name="firstName"
                       placeholder="User name"/>
                <#if firstNameError??>
                    <div class="invalid-feedback">
                        ${firstNameError}
                    </div>
                </#if>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Middle name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control ${(middleNameError??)?string('is-invalid', '')}"
                       value="<#if userInfo??>${userInfo.middleName}</#if>" name="middleName"
                       placeholder="User middle name"/>
                <#if middleNameError??>
                    <div class="invalid-feedback">
                        ${middleNameError}
                    </div>
                </#if>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6">
                <div class="g-recaptcha" data-sitekey="6LdsOasZAAAAAP0ygDD_nMBFVfW3DUbFsfSeNjYp"></div>
                <#if captchaError??>
                    <div class="alert alert-danger" role="alert">
                        ${captchaError}
                    </div>
                </#if>
            </div>
        </div>
        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <button class="btn btn-primary" type="submit">Create</button>
    </form>
</@c.page>
