<#import "macro/common.ftl" as c>
<#import "macro/pagination.ftl" as p>
<#include "include/security.ftl">

<@c.page>
    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>Doctors</strong>
    </h5>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <div>
        <div class="card-columns">
            <#list page.content as item>
                <!-- Card -->
                <div class="card card" style="width: max-content; max-width: 20rem">
                    <#if item.profileIconName??>
                        <img class="card-img-top" src="/img/${item.profileIconName}" alt="Card image cap"
                             style="max-width: 18rem">
                    <#else>
                        <img class="card-img-top" src="/static/icon/male-2-doctor.png" alt="Card image cap"
                             style="max-width: 18rem">
                    </#if>
                    <div class="card-body">
                        <h4 class="card-title"><a href="/doctor/${item.userAccountId}">${item.fullName}</a></h4>
                        <h6 class="card-subtitle mb-2 text-muted">${item.userAccountRole.roleName}</h6>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk
                            of the card's content.</p>
                    </div>
                </div>

            <#else>
                No doctors
            </#list>
        </div>
    </div>
    <hr class="hr-light my-4 wow fadeInDown" data-wow-delay="0.4s">
    <@p.pagination url page />

</@c.page>

