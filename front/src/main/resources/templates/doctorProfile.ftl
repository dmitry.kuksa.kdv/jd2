<#import "macro/common.ftl" as c>
<#import "macro/userEditForm.ftl" as u>
<#include "include/security.ftl">

<@c.page>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <div class="container my-5 py-5 z-depth-1">
        <!--Section: Content-->
        <section class="px-md-5 mx-md-5 text-center text-lg-left dark-grey-text">
            <div class="row">
                <div class="col-md-6 mb-4 mb-md-0">
                    <div class="view overlay z-depth-1-half">
                        <div class="w3-card-4" style="width:80%">
                            <#if userInfo.profileIconName??>
                                <img class="card-img-top" src="/img/${userInfo.profileIconName}" alt="Card image cap">
                            <#else>
                                <img class="card-img-top" src="/static/icon/male-2-doctor.png" alt="Card image cap">
                            </#if>
                            <div class="card-body">
                                <h5 class="card-title">${userInfo.userAccountRole.roleName}</h5>
                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                    <#if currentUserRoleCode == "admin">
                                        <a class="btn btn-outline-info text-info mr-2" data-toggle="collapse"
                                           data-target="#collapseExample"
                                           aria-expanded="false"
                                           aria-controls="collapseExample">
                                            Create timetable
                                        </a>
                                    </#if>
                                    <#if currentUserRoleCode == "patient">
                                        <a class="btn btn-outline-info text-info mr-2" data-toggle="collapse"
                                           data-target="#collapseExample4"
                                           aria-expanded="false"
                                           aria-controls="collapseExample4">
                                            Book an appointment
                                        </a>
                                    </#if>
                                    <#if currentUserRoleCode == "doctor" || currentUserRoleCode == "admin">
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupDrop" type="button"
                                                    class="btn btn-outline-info dropdown-toggle"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop">

                                                <a class="dropdown-item text-info" data-toggle="collapse"
                                                   data-target="#collapseExample3"
                                                   aria-expanded="false"
                                                   aria-controls="collapseExample3">
                                                    View appointments
                                                </a>
                                                <#if currentUserRoleCode == "patient">
                                                    <a class="dropdown-item text-info" data-toggle="collapse"
                                                       data-target="#collapseExample2"
                                                       aria-expanded="false"
                                                       aria-controls="collapseExample2">
                                                        Change password
                                                    </a>
                                                </#if>
                                            </div>
                                        </div>
                                    </#if>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-4 mb-md-0">

                    <form>
                        <div class="form-group row">
                            <h3 class="font-weight-bold">${userInfo.fullName}</h3>
                        </div>
                        <div class="form-group row">
                            <b><label for="phoneNumber" class="text-info">Phone number</label></b>
                            <input type="text" readonly class="form-control-plaintext" id="phoneNumber"
                                   value="${userInfo.userAccountPhoneNumber}">
                        </div>
                        <div class="form-group row">
                            <b><label for="email" class="text-info">Email</label></b>
                            <input type="text" readonly class="form-control-plaintext" id="email"
                                   value="${userInfo.userAccountEmail}">
                        </div>
                        <div class="form-group row">
                            <p class="text-muted">Some quick example text to build on the card title and make up the
                                bulk of the
                                card's content.
                        </div>
                    </form>
                </div>
            </div>
        </section>

        <div class="accordion" id="accordionExample">
            <#if currentUserRoleCode == "doctor">
                <div class="collapse <#if resetPasswordError??>show</#if>" id="collapseExample2"
                     data-parent="#accordionExample">
                    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
                        <strong>Change password</strong>
                    </h5>
                    <@u.resetPassword "/user/profile/changePassword" false/>
                </div>
            </#if>
            <#if currentUserRoleCode == "doctor" || currentUserRoleCode == "admin">
                <div class="collapse <#if date??>show</#if>" id="collapseExample3" data-parent="#accordionExample">
                    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
                        <strong>Appointments</strong>
                    </h5>
                    <#include "include/appointmentList.ftl">
                </div>
            </#if>

            <#if currentUserRoleCode == "patient">
                <div class="collapse <#if date??>show</#if>" id="collapseExample4" data-parent="#accordionExample">
                    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
                        <strong>Appointments</strong>
                    </h5>
                    <#include "include/appointmentTable.ftl">
                </div>
            </#if>

            <#if currentUserRoleCode == "admin">
                <div class="collapse" id="collapseExample" data-parent="#accordionExample">
                    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
                        <strong>Create timetable</strong>
                    </h5>
                    <#include "include/timetable.ftl">
                </div>
            </#if>
        </div>
    </div>
</@c.page>

