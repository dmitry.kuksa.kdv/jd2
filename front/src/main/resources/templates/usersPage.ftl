<#import "macro/common.ftl" as c>
<#import "macro/pagination.ftl" as p>
<#include "include/security.ftl">
<#import "macro/userEditForm.ftl" as u>

<@c.page>
    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>Users</strong>
    </h5>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <div class="d-flex bd-highlight mb-3">
        <div class="mr-auto p-2 bd-highlight">
            <#if currentUserRoleCode == "admin">
                <a class="btn btn-outline-info" href="/user/newUser" role="button">Add new user</a>
            </#if>
        </div>
        <div class="p-2 bd-highlight">
            <form class="form-inline my-2 my-lg-0">
                <label class="text-info mr-2" for="role">Role</label>
                <select name="roleCode" class="form-control mr-2" id="role">
                    <#list roleList as role>
                        <option value="${role.roleCode}" name="roleCode"
                                <#if roleCode?? && roleCode == role.roleCode>selected="selected"</#if>>${role.roleName}
                        </option>
                    </#list>
                </select>
                <input class="form-control mr-sm-2" type="search" name="fullName" placeholder="Search by name"
                       aria-label="Search">
                <#--                <input type="hidden" name="_csrf" value="${_csrf.token}"/>-->
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </div>

    <table class="table table-hover mt-2">
        <thead class="thead-light">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Login</th>
            <th scope="col">Role</th>
            <th scope="col">Status</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <#list page.content as item>
            <tr>
                <td>${item.fullName}</td>
                <td>${item.userLogin}</td>
                <td>${item.userAccountRole.roleName}</td>
                <td><#if item.isUserEnabled>Enable<#else>Disable</#if></td>
                <td>
                    <div class="dropdown">
                        <i class="btn fas fa-ellipsis-v" data-toggle="dropdown"
                           id="dropdownMenuLink${item.userAccountId}"></i>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink${item.userAccountId}">
                            <form>
                                <a class="dropdown-item" href="/user/${item.userAccountId}">View details</a>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        <#else>
            No users
        </#list>
        </tbody>
    </table>

    <hr class="hr-light my-4 wow fadeInDown" data-wow-delay="0.4s">
    <@p.pagination url page />
</@c.page>