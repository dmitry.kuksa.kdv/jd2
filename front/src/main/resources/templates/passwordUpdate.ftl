<#import "macro/common.ftl" as c>
<#import "macro/login.ftl" as l>

<@c.page>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <form action="/passwordUpdate/${code}" method="post">
        <div class="form-row">
            <!-- New password -->
            <div class=" form-group col-md-4">
                <label>Password</label>
                <input type="password" name="password"
                       class="form-control ${(passwordError??)?string('is-invalid', '')}"
                       placeholder="New password" required/>
                <#if passwordError??>
                    <div class="invalid-feedback">
                        ${passwordError}
                    </div>
                </#if>
            </div>
            <!-- Password confirm -->
            <div class="form-group col-md-4">
                <label>Password confirm</label>
                <input type="password" name="passwordConfirm"
                       class="form-control ${(passwordError??)?string('is-invalid', '')}"
                       placeholder="Password confirm" required/>
                <#if passwordError??>
                    <div class="invalid-feedback">
                        ${passwordError}
                    </div>
                </#if>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <button class="btn btn-outline-info btn-block mt-4" type="submit">Save</button>
        </div>
    </form>
</@c.page>

