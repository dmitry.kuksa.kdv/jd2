<#import "macro/common.ftl" as c>

<@c.page>
    <div class="bg"></div>
    <h1 class="h1-reponsive white-text text-uppercase font-weight-bold mb-0 pt-md-5 pt-5 wow fadeInDown"
        data-wow-delay="0.3s">
        <strong>Welcome</strong>
    </h1>
    <hr class="hr-light my-4 wow fadeInDown" data-wow-delay="0.4s">
    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>This is a medical center web application</strong>
    </h5>
</@c.page>