<#import "macro/common.ftl" as c>
<#import "macro/pagination.ftl" as p>
<#include "include/security.ftl">
<#import "macro/userEditForm.ftl" as u>

<@c.page>
    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>Medical Reports</strong>
    </h5>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>

    <table class="table table-hover mt-2">
        <thead class="thead-light">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Patient</th>
            <th scope="col">Doctor</th>
            <th scope="col">Date</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <#list page.content as item>
            <tr>
                <td style="width: 5%">${item.id}</td>
                <td style="width: 35%"><a href="/patient/${item.patient.userAccountId}">${item.patient.userAccountName}</a>
                <td style="width: 35%"><a href="/doctor/${item.doctor.userAccountId}">${item.doctor.userAccountName}</a>
                <td style="width: 20%">${item.creationDate}</td>
                <td style="width: 5%">
                    <div class="dropdown">
                        <i class="btn fas fa-ellipsis-v" data-toggle="dropdown"
                           id="dropdownMenuLink${item.id}"></i>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink${item.id}">
                            <form>
                                <a class="dropdown-item" href="/report/${item.id}">View details</a>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        <#else>
            No reports
        </#list>
        </tbody>
    </table>

    <hr class="hr-light my-4 wow fadeInDown" data-wow-delay="0.4s">
    <@p.pagination url page />
</@c.page>