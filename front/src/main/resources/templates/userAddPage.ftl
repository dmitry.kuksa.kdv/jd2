<#import "macro/common.ftl" as c>
<#import "macro/userEditForm.ftl" as u>
<#include "include/security.ftl">

<@c.page>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>Create user</strong>
    </h5>
        <@u.userEditForm "/user" false/>
    </div>
</@c.page>

