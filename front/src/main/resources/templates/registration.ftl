<#import "macro/common.ftl" as c>
<#import "macro/login.ftl" as l>
<@c.page>
    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>Registration</strong>
    </h5>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <div class="border border-light p-5">
        <form action="/registration" method="post">
            <div class="form-row">
                <!-- Login -->
                <div class="form-group col-md-4">
                    <label for="login">Login</label>
                    <input type="text" name="userLogin" id="login" value="<#if userInfo??>${userInfo.userLogin}</#if>"
                           class="form-control ${(userLoginError??)?string('is-invalid', '')}"
                           placeholder="User login"/>
                    <#if userLoginError??>
                        <div class="invalid-feedback">
                            ${userLoginError}
                        </div>
                    </#if>
                </div>
                <!-- Password -->
                <div class="form-group col-md-4">
                    <label>Password</label>
                    <input type="password" name="password"
                           class="form-control ${(passwordError??)?string('is-invalid', '')}"
                           placeholder="Password"/>
                    <#if passwordError??>
                        <div class="invalid-feedback">
                            ${passwordError}
                        </div>
                    </#if>
                </div>
                <!-- Password confirm -->
                <div class="form-group col-md-4">
                    <label>Password confirm</label>
                    <input type="password" name="passwordConfirm"
                           class="form-control ${(passwordError??)?string('is-invalid', '')}"
                           placeholder="Password confirm"/>
                    <#if passwordError??>
                        <div class="invalid-feedback">
                            ${passwordError}
                        </div>
                    </#if>
                </div>
            </div>
            <div class="form-row">
                <!-- Surname -->
                <div class="form-group col-md-4">
                    <label>Surname</label>
                    <input type="text" class="form-control ${(lastNameError??)?string('is-invalid', '')}"
                           value="<#if userInfo??>${userInfo.lastName}</#if>" name="lastName"
                           placeholder="User surname"/>
                    <#if lastNameError??>
                        <div class="invalid-feedback">
                            ${lastNameError}
                        </div>
                    </#if>
                </div>
                <!-- Name -->
                <div class="form-group col-md-4">
                    <label>Name</label>
                    <input type="text" class="form-control ${(firstNameError??)?string('is-invalid', '')}"
                           value="<#if userInfo??>${userInfo.firstName}</#if>" name="firstName"
                           placeholder="User name"/>
                    <#if firstNameError??>
                        <div class="invalid-feedback">
                            ${firstNameError}
                        </div>
                    </#if>
                </div>
                <!-- Middle name -->
                <div class="form-group col-md-4">
                    <label>Middle name</label>
                    <input type="text" class="form-control ${(middleNameError??)?string('is-invalid', '')}"
                           value="<#if userInfo??>${userInfo.middleName}</#if>" name="middleName"
                           placeholder="User middle name"/>
                    <#if middleNameError??>
                        <div class="invalid-feedback">
                            ${middleNameError}
                        </div>
                    </#if>
                </div>
            </div>
            <div class="form-row">
                <!-- Email -->
                <div class="form-group col-md-6">
                    <label>Email</label>
                    <input type="email" class="form-control ${(userAccountEmailError??)?string('is-invalid', '')}"
                           value="<#if userInfo??>${userInfo.userAccountEmail}</#if>" name="userAccountEmail"
                           placeholder="temp@gmail.com"/>
                    <#if userAccountEmailError??>
                        <div class="invalid-feedback">
                            ${userAccountEmailError}
                        </div>
                    </#if>
                </div>
                <!-- Phone number -->
                <div class="form-group col-md-6">
                    <label>Phone number</label>
                    <input type="text" class="form-control" name="userAccountPhoneNumber"
                           placeholder="Phone number"/>
                </div>
            </div>

            <!--Captcha-->
            <div class="form-row justify-content-md-start">
                <div class="g-recaptcha" data-sitekey="6LdsOasZAAAAAP0ygDD_nMBFVfW3DUbFsfSeNjYp"></div>
                <#if captchaError??>
                    <div class="alert alert-danger" role="alert">
                        ${captchaError}
                    </div>
                </#if>
            </div>

            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <button class="btn btn-outline-info btn-block mt-4" type="submit">Create</button>
        </form>
    </div>
</@c.page>