<div class="d-flex bd-highlight mb-3">
    <div class="ml-auto p-2 bd-highlight">
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" name="date" type="date" value="<#if date??>${date}<#else>${currentDate}</#if>" aria-label="Date">
            <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</div>

<table class="table table-hover mt-2">
    <thead class="thead-light">
    <tr>
        <th scope="col">Time</th>
        <th scope="col">Patient</th>
        <th scope="col">Status</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <#list appointmentList as item>
        <tr>
            <td style="width: 42%">${item.appointmentTime}</td>
            <td style="width: 35%">
                <#if item.patientFullName??>
                    <a href="/patient/${item.patientId}">${item.patientFullName}</a>
                <#else>
                    -
                </#if>
            </td>
            <td style="width: 20%">${item.status}</td>
            <td style="width: 3%">
                <div class="dropdown">
                    <i class="btn fas fa-ellipsis-v" data-toggle="dropdown"
                       id="dropdownMenuLink${item.id}"></i>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink${item.id}">
                        <form>
                            <a class="dropdown-item" href="#">No actions</a>
                        </form>
                        <#--                    <form action="/report/newReport" method="get">-->
                        <#--                        <input type="hidden" name="patientId" value="${item.userAccountId}"/>-->
                        <#--                        <button class="dropdown-item" type="submit">Create report</button>-->
                        <#--                    </form>-->
                    </div>
                </div>
            </td>
        </tr>
    <#else>
        No patients
    </#list>
    </tbody>
</table>