<#include "security.ftl">
<#import "../macro/login.ftl" as l>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Medical center</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/doctor?page=0&size=10">Doctors</a>
            </li>
            <#if currentUserInfo??>
                <#if currentUserRoleCode == "doctor" || currentUserRoleCode == "manager">
                    <li class="nav-item">
                        <a class="nav-link" href="/report?page=0&size=10">Reports</a>
                    </li>
                </#if>
                <#if currentUserRoleCode == "doctor" || currentUserRoleCode == "manager">
                    <li class="nav-item">
                        <a class="nav-link" href="/patient?page=0&size=10">Patients</a>
                    </li>
                </#if>
                <#if currentUserRoleCode == "admin" || currentUserRoleCode == "manager">
                    <li class="nav-item">
                        <a class="nav-link" href="/user?page=0&size=10">Users</a>
                    </li>
                </#if>
            </#if>
        </ul>

        <a class="navbar-text mr-2" href="/user/profile">${currentUserFullName!""}</a>
        <#if currentUserInfo??>
            <form action="/logout" method="post">
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <button class="btn btn-outline-danger" type="submit">Sign Out</button>
            </form>
        <#else>
            <a class="btn btn-outline-primary" href="/login">Sign In</a>
        </#if>

    </div>
</nav>