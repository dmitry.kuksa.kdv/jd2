<table class="table table-hover mt-2">
    <thead class="thead-light">
    <tr>
        <th scope="col">Doctor</th>
        <th scope="col">Date</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <#if userMedicalReportList??>
        <#list userMedicalReportList as item>
            <tr>
                <td style="width: 55%"><a href="/doctor/${item.doctor.userAccountId}">${item.doctor.userAccountName}</a>
                </td>
                <td style="width: 52%">${item.creationDate}</td>
                <td style="width: 3%">
                    <div class="dropdown">
                        <i class="btn fas fa-ellipsis-v" data-toggle="dropdown"
                           id="dropdownMenuLink${item.id}"></i>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink${item.id}">
                            <form>
                                <a class="dropdown-item" href="/report/${item.id}">View details</a>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>

        </#list>
    <#else>
        No reports
    </#if>
    </tbody>
</table>