<#assign
known = Session.SPRING_SECURITY_CONTEXT??
>

<#if known>
    <#assign
    currentUserInfo = Session.SPRING_SECURITY_CONTEXT.authentication.principal
    currentUserFullName = currentUserInfo.getFullName()
    currentUserRoleCode = currentUserInfo.getUserAccountRole().getRoleCode()
    currentUserAccountId = currentUserInfo.getUserAccountId()
    >
<#else>
    <#assign
    name = "unknown"
    currentUserRoleCode = "none"
    currentUserAccountId = -1
    >
</#if>