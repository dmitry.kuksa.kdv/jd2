<div class="d-flex bd-highlight mb-3">
    <div class="ml-auto p-2 bd-highlight">
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" name="date" type="date"
                   value="<#if date??>${date}<#else>${currentDate}</#if>"
                   aria-label="Date">
            <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</div>

<table class="table table-hover mt-2">
    <thead class="thead-light">
    <tr>
        <th scope="col">Time</th>
        <th scope="col">Doctor</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <#list appointmentListForTable as item>
        <tr>
            <td style="width: 47%">${item.appointmentTime}</td>
            <td style="width: 50%">${item.doctorFullName!' - '}</td>
            <td style="width: 3%">
                <div class="dropdown">
                    <i class="btn fas fa-ellipsis-v" data-toggle="dropdown"
                       id="dropdownMenuLink${item.id}"></i>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink${item.id}">
                        <form action="/appointment" method="post">
                            <input type="hidden" name="appointmentId" value="${item.id}"/>
                            <input type="hidden" name="patientId" value="${currentUserAccountId}"/>
                            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                            <button class="dropdown-item" type="submit">Book an appointment</button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
    <#else>
        No appointments
    </#list>
    </tbody>
</table>