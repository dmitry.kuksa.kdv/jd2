<form action="/doctor/timetable" method="post">
    <div class="form-row">
        <div class="form-group col-md-3">
            <label>Date</label>
            <input class="form-control" name="date" type="date" value="<#if date??>${date}<#else>${currentDate}</#if>">
        </div>
    </div>
    <div class="form-row">
        <#list timeList as item>
            <div class="col-xl-1 p-2">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="time::${item}" class="custom-control-input" id="${item}"
                           value="${item}">
                    <label class="custom-control-label" for="${item}">${item}</label>
                </div>
            </div>
        <#else>
            No time
        </#list>
    </div>
    <input type="hidden" name="_csrf" value="${_csrf.token}"/>
    <input type="hidden" name="doctorId" value="${userInfo.userAccountId}"/>
    <button class="btn btn-outline-info btn-block mt-4" type="submit">Add</button>
</form>
