<table class="table table-hover mt-2">
    <thead class="thead-light">
    <tr>
        <th scope="col">Date</th>
        <th scope="col">Time</th>
        <th scope="col">Doctor</th>
    </tr>
    </thead>
    <tbody>
    <#list userAppointmentList as item>
        <tr>
            <td style="width: 30%">${item.appointmentFullDate}</td>
            <td style="width: 30%">${item.appointmentTime}</td>
            <td style="width: 40%">${item.doctorFullName}</td>
        </tr>
    <#else>
        No appointments
    </#list>
    </tbody>
</table>