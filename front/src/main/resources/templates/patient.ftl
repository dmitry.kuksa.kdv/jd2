<#import "macro/common.ftl" as c>
<#import "macro/pagination.ftl" as p>
<#include "include/security.ftl">

<@c.page>
    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>Patients</strong>
    </h5>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <div class="d-flex bd-highlight mb-3">
        <div class="ml-auto p-2 bd-highlight">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" name="fullName" placeholder="Search by name"
                       aria-label="Search">
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </div>

    <table class="table table-hover mt-2">
        <thead class="thead-light">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone number</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <#list page.content as item>
            <tr>
                <td style="width: 42%">${item.fullName}</td>
                <td style="width: 35%">${item.userAccountEmail}</td>
                <td style="width: 20%">${item.userAccountPhoneNumber}</td>
                <td style="width: 3%">
                    <div class="dropdown">
                        <i class="btn fas fa-ellipsis-v" data-toggle="dropdown"
                           id="dropdownMenuLink${item.userAccountId}"></i>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink${item.userAccountId}">
                            <form>
                                <a class="dropdown-item" href="/patient/${item.userAccountId}">View Details</a>
                            </form>
                            <form>
                                <a class="dropdown-item" href="/patient/${item.userAccountId}/addReport">Create
                                    report</a>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        <#else>
            No patients
        </#list>
        </tbody>
    </table>

    <hr class="hr-light my-4 wow fadeInDown" data-wow-delay="0.4s">
    <@p.pagination url page />
</@c.page>