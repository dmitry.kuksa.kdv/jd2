<#import "macro/common.ftl" as c>
<#import "macro/userEditForm.ftl" as u>
<#include "include/security.ftl">

<@c.page>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>Create medical report</strong>
    </h5>
    <h6 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>${currentDate}</strong>
    </h6>

    <form action="/report" method="post" enctype="multipart/form-data">
        <div class="form-row">
            <div class="form-group col-md-12">
                <label><strong>Patient</strong></label>

                <u class="text-info">${patient.fullName}</u>

            </div>
        </div>
        <div class="form-row">
            <!-- Diagnosis -->
            <div class="form-group col-md-12">
                <label><strong>Diagnosis</strong></label>
                <textarea class="form-control ${(diagnosisError??)?string('is-invalid', '')}"
                          value="<#if medicalReport??>${medicalReport.diagnosis}</#if>" name="diagnosis" rows="4">
                </textarea>
                <#if diagnosisError??>
                    <div class="invalid-feedback">
                        ${diagnosisError}
                    </div>
                </#if>
            </div>
        </div>
        <div class="form-row">
            <!-- Prescription -->
            <div class="form-group col-md-6">
                <label><strong>Prescription</strong></label>
                <textarea class="form-control ${(prescriptionError??)?string('is-invalid', '')}"
                          value="<#if medicalReport??>${medicalReport.prescription}</#if>" name="prescription" rows="5">
                </textarea>
                <#if prescriptionError??>
                    <div class="invalid-feedback">
                        ${prescriptionError}
                    </div>
                </#if>
            </div>
            <!-- Survey -->
            <div class="form-group col-md-6">
                <label><strong>Survey</strong></label>
                <textarea class="form-control ${(surveyError??)?string('is-invalid', '')}"
                          value="<#if medicalReport??>${medicalReport.survey}</#if>" name="survey" rows="5">
                </textarea>
                <#if surveyError??>
                    <div class="invalid-feedback">
                        ${surveyError}
                    </div>
                </#if>
            </div>
        </div>
        <div class="form-row">
            <!-- Recommendation -->
            <div class="form-group col-md-12">
                <label><strong>Recommendation</strong></label>
                <textarea class="form-control ${(recommendationError??)?string('is-invalid', '')}"
                          value="<#if medicalReport??>${medicalReport.recommendation}</#if>" name="recommendation"
                          rows="4">
                </textarea>
                <#if recommendationError??>
                    <div class="invalid-feedback">
                        ${recommendationError}
                    </div>
                </#if>
            </div>
        </div>
        <div class="form-row">
            <!-- attachmentFileName -->
            <div class="form-group col-md-4">
                <div class="custom-file">
                    <label for="exampleFormControlFile1">Upload file</label>
                    <input type="file" class="form-control-input" id="exampleFormControlFile1" name="file">
                </div>
            </div>
        </div>
        <!-- patient -->
        <input type="hidden" name="patientId" value="${patient.userAccountId}"/>

        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <button class="btn btn-outline-info btn-block mt-4" type="submit">Create</button>
    </form>
</@c.page>

