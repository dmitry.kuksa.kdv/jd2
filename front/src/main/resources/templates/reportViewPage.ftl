<#import "macro/common.ftl" as c>
<#import "macro/userEditForm.ftl" as u>
<#include "include/security.ftl">


<@c.page>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>

    <div class="card border-secondary p-3">
        <div class="card-header bg-white border-info">
            <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
                <strong>Medical report</strong>
            </h5>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <!-- Patient -->
                <div class="card border-0 mb-1">
                    <div class="card-body">
                        <h5 class="card-title text-info"><strong>Patient</strong></h5>
                        <p class="card-text"><u>${medicalReport.patient.userAccountName}</u></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <!-- Diagnosis -->
                <div class="card border-0 mb-2">
                    <div class="card-body">
                        <h5 class="card-title text-info"><strong>Diagnosis</strong></h5>
                        <p class="card-text">${medicalReport.diagnosis}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <!-- Prescription -->
                <div class="card border-0 mb-2">
                    <div class="card-body">
                        <h5 class="card-title text-info"><strong>Prescription</strong></h5>
                        <p class="card-text">${medicalReport.prescription}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <!-- Survey -->
                <div class="card border-0 mb-2">
                    <div class="card-body">
                        <h5 class="card-title text-info"><strong>Survey</strong></h5>
                        <p class="card-text">${medicalReport.survey}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <!-- Recommendation -->
                <div class="card border-0 mb-2">
                    <div class="card-body">
                        <h5 class="card-title text-info"><strong>Recommendation</strong></h5>
                        <p class="card-text">${medicalReport.recommendation}</p>
                    </div>
                </div>
            </div>
        </div>
<#--        <hr class="border-info">-->
        <div class="row">
            <div class="col-sm-6">
                <!-- Doctor -->
                <div class="card border-0 mb-1">
                    <div class="card-body">
                        <h5 class="card-title text-info"><strong>Doctor</strong></h5>
                        <p class="card-text"><u>${medicalReport.doctor.userAccountName}</u></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <!-- Date -->
                <div class="card border-0 mb-1">
                    <div class="card-body">
                        <h5 class="card-title text-info"><strong>Date</strong></h5>
                        <p class="card-text"><u>${medicalReport.creationDate}</u></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</@c.page>