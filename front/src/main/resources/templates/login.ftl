<#import "macro/common.ftl" as c>
<#import "macro/login.ftl" as l>

<@c.page>
    <#if Session?? && Session.SPRING_SECURITY_LAST_EXCEPTION??>
        <div class="alert alert-danger" role="alert">
            ${Session.SPRING_SECURITY_LAST_EXCEPTION.message}
        </div>
    </#if>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <form class="text-center border border-light p-5" action="/login" method="post">
        <p class="h4 mb-4">Sign in</p>
        <div class="row justify-content-md-center">
            <div class="col-sm-7">
                <!-- Login -->
                <input type="text" name="username" value="<#if user??>${user.username}</#if>"
                       class="form-control mb-4 ${(usernameError??)?string('is-invalid', '')}" placeholder="Login"
                       required>
                <#if usernameError??>
                    <div class="invalid-feedback">
                        ${usernameError}
                    </div>
                </#if>
                <!-- Password -->

                <input type="password" name="password" class="form-control mb-1" placeholder="Password" required>
                <p class="font-small grey-text d-flex justify-content-end"><a href="/passwordReset">Forgot password?</a></p>

                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <button class="btn btn-outline-info btn-block mt-4" type="submit">Sign in</button>
                <hr>
                <p>Not a member?
                    <a href="/registration">Register</a>
            </div>
        </div>
    </form>
</@c.page>