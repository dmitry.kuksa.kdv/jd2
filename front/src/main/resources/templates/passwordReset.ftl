<#import "macro/common.ftl" as c>
<#import "macro/login.ftl" as l>

<@c.page>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <form action="/passwordReset" method="post">
        <div class="form-row">
            <!-- Email -->
            <div class=" form-group col-md-4">
                <label>Email</label>
                <input type="email" name="email"
                       class="form-control"
                       placeholder="temp@gmail.com" required/>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <button class="btn btn-outline-info btn-block mt-4" type="submit">Send reset code</button>
        </div>
    </form>
</@c.page>

