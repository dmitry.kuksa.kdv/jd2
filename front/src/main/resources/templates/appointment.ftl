<#import "macro/common.ftl" as c>
<#include "include/security.ftl">

<@c.page>
    <h5 class="text-uppercase mb-4 white-text wow fadeInDown" data-wow-delay="0.4s">
        <strong>Medical Reports</strong>
    </h5>
    <#if message??>
        <div class="alert alert-${messageType}" role="alert">
            ${message}
        </div>
    </#if>
    <div class="d-flex bd-highlight mb-3">
        <div class="ml-auto p-2 bd-highlight">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" name="date" type="date" value="<#if date??>${date}</#if>"
                       aria-label="Date">
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </div>

    <table class="table table-hover mt-2">
        <thead class="thead-light">
        <tr>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Patient</th>
            <th scope="col">Doctor</th>
            <th scope="col">Status</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <#list appointmentList as item>
            <tr>
                <td style="width: 20%">${item.appointmentDate}</td>
                <td style="width: 10%">${item.appointmentTime}</td>
                <td style="width: 30%">${item.patientFullName!' - '}</td>
                <td style="width: 30%">${item.doctorFullName}</td>
                <td style="width: 10%">${item.status}</td>
            </tr>
        <#else>
            No appointments
        </#list>
        </tbody>
    </table>
    <hr class="hr-light my-4 wow fadeInDown" data-wow-delay="0.4s">
</@c.page>