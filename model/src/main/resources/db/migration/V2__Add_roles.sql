INSERT INTO public.core_role (role_id, role_code, role_name, role_description, is_active, category_name)
VALUES (1, 'admin', 'Administrator', 'Admin user registers users with the role category Employee', true, 'Employee');
INSERT INTO public.core_role (role_id, role_code, role_name, role_description, is_active, category_name)
VALUES (2, 'manager', 'Manager', 'Manager user registers users with the role category Patient', true, 'Employee');
INSERT INTO public.core_role (role_id, role_code, role_name, role_description, is_active, category_name)
VALUES (3, 'doctor', 'Doctor', 'Doctor user diagnoses and prescribes treatment to Patient users', true, 'Employee');
INSERT INTO public.core_role (role_id, role_code, role_name, role_description, is_active, category_name)
VALUES (4, 'patient', 'Patient', 'Patient user makes an appointment with doctors', true, 'Patient');