INSERT INTO public.core_user (user_id, activation_code, first_name, full_name, is_enabled, last_name, middle_name, user_login, user_password)
VALUES (0, null, 'Mega', 'Super Mega Admin', true, 'Super', 'Admin', 'admin', 'admin');

INSERT INTO public.core_user_account (user_account_id, user_account_name, user_account_email)
VALUES (0, 'Super Mega Admin', 'medcenterjd2@gmail.com');

INSERT INTO public.core_user_account_role_link (user_account_role_link_id, user_account_id, role_id)
VALUES (0, 0, 1);

INSERT INTO public.core_user_account_user_link (user_account_user_link_id, user_account_id, user_id)
VALUES (0, 0, 0);

CREATE extension if NOT EXISTS pgcrypto;
UPDATE public.core_user
SET user_password = crypt(user_password, gen_salt('bf', 8));