package com.dvkuksa.model.dal.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "medical_report")
public class MedicalReport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @NotBlank(message = "Required field")
    @Length(max = 2048, message = "Diagnosis too long")
    @Column(name = "diagnosis")
    private String diagnosis;

    @NotBlank(message = "Required field")
    @Length(max = 2048, message = "Prescription too long")
    @Column(name = "prescription")
    private String prescription;

    @NotBlank(message = "Required field")
    @Length(max = 2048, message = "Survey too long")
    @Column(name = "survey")
    private String survey;

    @NotBlank(message = "Required field")
    @Length(max = 2048, message = "Recommendation too long")
    @Column(name = "recommendation")
    private String recommendation;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_by")
    private UserAccount doctor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private UserAccount patient;

    @Column(name = "attachment_file_name")
    private String attachmentFileName;
}
