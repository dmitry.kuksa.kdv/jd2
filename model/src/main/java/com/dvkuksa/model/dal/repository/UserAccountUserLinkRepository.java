package com.dvkuksa.model.dal.repository;

import com.dvkuksa.model.dal.entity.UserAccountUserLink;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAccountUserLinkRepository extends CrudRepository<UserAccountUserLink, Long> {

    UserAccountUserLink findByUserAccountId(Long userAccountId);

    UserAccountUserLink findByUserId(Long userId);

    List<UserAccountUserLink> findAllByUserAccountIdIn(List<Long> userAccountIdList);

    UserAccountUserLink save(UserAccountUserLink userAccountUserLink);
}
