package com.dvkuksa.model.dal.repository;

import com.dvkuksa.model.dal.entity.UserAccount;
import com.dvkuksa.model.dal.entity.UserAccountDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAccountDetailRepository extends CrudRepository<UserAccountDetail, Long> {
    List<UserAccountDetail> findAllByDetailAddressAndUserAccount(String detailAddress, UserAccount userAccount);
}
