package com.dvkuksa.model.dal.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "core_user")
@NamedEntityGraph(name = User.EG_FULL, includeAllAttributes = true)
public class User implements Serializable {

    private static final long serialVersionUID = -8880321476283054758L;
    public static final String EG_FULL = "UserEntity";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long id;

    @Column(name = "user_login", unique = true)
    private String userLogin;

    @Column(name = "user_password", nullable = false)
    private String userPassword;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "is_enabled")
    private Boolean isEnabled;

    @Column(name = "activationCode")
    private String activationCode;
}
