package com.dvkuksa.model.dal.repository;

import com.dvkuksa.model.dal.entity.Appointment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AppointmentScheduleRepository extends CrudRepository<Appointment, Long> {

    List<Appointment> findAll();

    Appointment findAppointmentScheduleById(Long id);

    List<Appointment> findAllByPatientUserAccountId(Long patientId);

    List<Appointment> findAllByAppointmentDateBetween(LocalDateTime dateFrom, LocalDateTime dateTo);

    List<Appointment> findAllByAppointmentDateBetweenAndDoctorUserAccountId(LocalDateTime dateFrom, LocalDateTime dateTo, Long doctorId);
}
