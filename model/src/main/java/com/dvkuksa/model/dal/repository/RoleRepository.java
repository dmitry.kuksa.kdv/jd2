package com.dvkuksa.model.dal.repository;

import com.dvkuksa.model.dal.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findByRoleCode(String roleCode);

    Role findRoleByRoleId(Long id);

    List<Role> findAllByRoleIdIn(List<Long> roleIdList);

    List<Role> findAll();

    Role save(Role role);
}
