package com.dvkuksa.model.dal.repository;

import com.dvkuksa.model.dal.entity.MedicalReport;
import com.dvkuksa.model.dal.entity.UserAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicalReportRepository extends CrudRepository<MedicalReport, Long> {

    Page<MedicalReport> findAll(Pageable page);

    MedicalReport findMedicalReportById(Long medicalReportId);

    Page<MedicalReport> findAllByDoctor(Pageable page, UserAccount doctor);

    Page<MedicalReport> findAllByPatient(Pageable page, UserAccount patient);

    List<MedicalReport> findAllByPatientUserAccountId(Long patientId);
}
