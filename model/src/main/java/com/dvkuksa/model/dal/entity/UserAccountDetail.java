package com.dvkuksa.model.dal.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "core_user_account_detail")
public class UserAccountDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "detail_id")
    private Long detailId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_account_id")
    private UserAccount userAccount;

    @Column(name = "detail_address")
    private String detailAddress;

    @Column(name = "detail_value")
    private String detailValue;
}
