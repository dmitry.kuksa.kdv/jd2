package com.dvkuksa.model.dal.repository;

import com.dvkuksa.model.dal.entity.UserAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAccountRepository extends CrudRepository<UserAccount, Long> {
    UserAccount findUserAccountByUserAccountId(Long id);

    Page<UserAccount> findUserAccountByUserAccountIdIn(Pageable page, List<Long> ids);

    Page<UserAccount> findAll(Pageable page);

    void delete(UserAccount userAccount);

    List<UserAccount> save(List<UserAccount> userAccounts);

    UserAccount findUserAccountsByUserAccountEmail(String email);
}
