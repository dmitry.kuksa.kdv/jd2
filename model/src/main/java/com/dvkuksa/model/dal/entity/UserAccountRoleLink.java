package com.dvkuksa.model.dal.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "core_user_account_role_link")
@NamedEntityGraph(name = UserAccountRoleLink.EG_FULL, includeAllAttributes = true)
public class UserAccountRoleLink implements Serializable {

    private static final long serialVersionUID = 6830385330084194733L;
    public static final String EG_FULL = "UserAccountRoleLinkEntity";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_account_role_link_id")
    private Long id;

    @Column(name = "user_account_id")
    private Long userAccountId;

    @Column(name = "role_id")
    private Long roleId;
}
