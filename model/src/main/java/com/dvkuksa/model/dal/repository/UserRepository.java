package com.dvkuksa.model.dal.repository;

import com.dvkuksa.model.dal.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByUserLogin(String login);

    User findByUserLoginAndUserPassword(String login, String password);

    List<User> findAllByIdIn(List<Long> userIdList);

    User findUserById(Long id);

    User save(User user);

    Boolean existsByUserLogin(String login);

    List<User> save(List<User> userList);

    User findByActivationCode(String activationCode);
}
