package com.dvkuksa.model.dal.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "core_user_account")
@NamedEntityGraph(name = UserAccount.EG_FULL, includeAllAttributes = true)
public class UserAccount implements Serializable {

    private static final long serialVersionUID = 767305148011776164L;
    public static final String EG_FULL = "UserAccountEntity";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_account_id")
    private Long userAccountId;

    @Column(name = "user_account_name")
    private String userAccountName;

    @Column(name = "user_account_email")
    private String userAccountEmail;

    @Column(name = "user_account_phone")
    private String userAccountPhoneNumber;

    @Column(name = "user_account_profile_icon_name")
    private String profileIconName;
}
