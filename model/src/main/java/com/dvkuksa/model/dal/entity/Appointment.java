package com.dvkuksa.model.dal.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "appointment")
public class Appointment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_by")
    private UserAccount doctor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private UserAccount patient;

    @Column(name = "appointment_date")
    private LocalDateTime appointmentDate;

    @Column(name = "is_registered")
    private Boolean isRegistered;

    @Column(name = "appointment_status")
    private String status;
}
