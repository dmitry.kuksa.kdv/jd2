package com.dvkuksa.model.dal.entity;

import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;

@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Data
@Table(name = "core_role")
@NamedEntityGraph(name = Role.EG_FULL)
public class Role implements Serializable, GrantedAuthority {

    private static final long serialVersionUID = -8840365222090926154L;
    public static final String EG_FULL = "RoleEntity";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    private Long roleId;

    @Column(name = "role_code", unique = true)
    private String roleCode;

    @Column(name = "role_name")
    private String roleName;

    @Column(name = "role_description")
    private String roleDescription;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "category_name")
    private String categoryCode;

    @Override
    public String getAuthority() {
        return this.roleCode;
    }
}
