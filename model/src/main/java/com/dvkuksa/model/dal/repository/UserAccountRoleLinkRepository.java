package com.dvkuksa.model.dal.repository;

import com.dvkuksa.model.dal.entity.UserAccountRoleLink;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAccountRoleLinkRepository extends CrudRepository<UserAccountRoleLink, Long> {

    UserAccountRoleLink findByUserAccountId(Long userAccountId);

    List<UserAccountRoleLink> findAllByUserAccountIdIn(List<Long> userAccountIdList);

    List<UserAccountRoleLink> findAllByRoleId(Long roleId);

    UserAccountRoleLink save(UserAccountRoleLink userAccountRoleLink);
}
