package com.dvkuksa.model.dal.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "core_user_account_user_link")
@NamedEntityGraph(name = UserAccountUserLink.EG_FULL, includeAllAttributes = true)
public class UserAccountUserLink implements Serializable {

    private static final long serialVersionUID = -1479897462905716957L;
    public static final String EG_FULL = "UserAccountUserLinkEntity";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_account_user_link_id")
    private Long id;

    @Column(name = "user_account_id")
    private Long userAccountId;

    @Column(name = "user_id")
    private Long userId;
}
