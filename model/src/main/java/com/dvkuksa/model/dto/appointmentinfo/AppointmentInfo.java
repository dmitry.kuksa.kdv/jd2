package com.dvkuksa.model.dto.appointmentinfo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AppointmentInfo {

    private Long id;
    private Long doctorId;
    private String doctorFullName;
    private Long patientId;
    private String patientFullName;
    private LocalDateTime appointmentDate;
    private String appointmentTime;
    private String appointmentFullDate;
    private Boolean isRegistered;
    private String status;
}
