package com.dvkuksa.model.dto.userinfo;

import com.dvkuksa.model.dal.entity.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class UserInfoMapper {

    public UserInfo buildUserInfo(User user,
                                  UserAccount userAccount,
                                  Role userAccountRole) {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(user.getId());
        userInfo.setUserLogin(user.getUserLogin());
        userInfo.setUserAccountId(userAccount.getUserAccountId());
        userInfo.setFirstName(user.getFirstName());
        userInfo.setLastName(user.getLastName());
        userInfo.setMiddleName(user.getMiddleName());
        userInfo.setFullName(user.getFullName());
        userInfo.setPassword(user.getUserPassword());
        userInfo.setUserAccountEmail(userAccount.getUserAccountEmail());
        userInfo.setUserAccountPhoneNumber(userAccount.getUserAccountPhoneNumber());
        userInfo.setProfileIconName(userAccount.getProfileIconName());
        userInfo.setIsUserEnabled(user.getIsEnabled());
        if (userAccountRole != null) {
            userInfo.setUserAccountRole(userAccountRole);
        }

        return userInfo;
    }

    public List<UserInfo> buildUserInfoList(List<User> userList,
                                            List<UserAccount> userAccountList,
                                            List<UserAccountUserLink> userAccountUserLinkList,
                                            List<Role> roleList,
                                            List<UserAccountRoleLink> userAccountRoleLinkList
    ) {
        List<UserInfo> userInfoList = new ArrayList<>();
        for (UserAccount userAccount : userAccountList) {
            Long userId = userAccountUserLinkList.stream()
                    .filter(p -> Objects.equals(p.getUserAccountId(), userAccount.getUserAccountId()))
                    .findAny()
                    .map(UserAccountUserLink::getUserId)
                    .orElse(null);
            User user = userList.stream()
                    .filter(p -> Objects.equals(p.getId(), userId))
                    .findAny()
                    .orElse(null);
            Long userAccountRoleId = userAccountRoleLinkList.stream()
                    .filter(p -> Objects.equals(p.getUserAccountId(), userAccount.getUserAccountId()))
                    .findAny()
                    .map(UserAccountRoleLink::getRoleId)
                    .orElse(null);
            Role userAccountRole = roleList.stream()
                    .filter(p -> Objects.equals(p.getRoleId(), userAccountRoleId))
                    .findAny()
                    .orElse(null);

            if (user != null) {
                userInfoList.add(buildUserInfo(user, userAccount, userAccountRole));
            }
        }
        return userInfoList;
    }
}
