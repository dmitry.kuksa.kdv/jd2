package com.dvkuksa.model.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.util.Set;

@Data
public class ReCaptchaResponse {
    private Boolean success;
    @JsonAlias("error-codes")
    private Set<String> errorCodes;
}
