package com.dvkuksa.model.dto.userinfo;


import com.dvkuksa.model.dal.entity.Role;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.Collections;

@Data
public class UserInfo implements UserDetails {

    private Long userId;

    @NotBlank(message = "Required field")
    private String userLogin;

    private Long userAccountId;

    @NotBlank(message = "Required field")
    private String firstName;

    @NotBlank(message = "Required field")
    private String lastName;

    @NotBlank(message = "Required field")
    private String middleName;

    private String fullName;

    private String password;

    private Boolean isUserEnabled;

    @Email(message = "Invalid email")
    @NotBlank(message = "Required field")
    private String userAccountEmail;

    @NotBlank(message = "Required field")
    private String userAccountPhoneNumber;

    private String profileIconName;

    private Role userAccountRole;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(this.userAccountRole);
    }

    @Override
    public String getUsername() {
        return this.userLogin;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.isUserEnabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.isUserEnabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.isUserEnabled;
    }

    @Override
    public boolean isEnabled() {
        return this.isUserEnabled;
    }
}
