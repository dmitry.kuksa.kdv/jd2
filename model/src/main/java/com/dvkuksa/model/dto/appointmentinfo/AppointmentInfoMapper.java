package com.dvkuksa.model.dto.appointmentinfo;

import com.dvkuksa.model.utils.DateHelper;
import com.dvkuksa.model.dal.entity.Appointment;

public class AppointmentInfoMapper {

    public static AppointmentInfo buildAppointmentInfo(Appointment appointment) {
        AppointmentInfo appointmentInfo = new AppointmentInfo();
        appointmentInfo.setId(appointment.getId());
        appointmentInfo.setDoctorId(appointment.getDoctor().getUserAccountId());
        appointmentInfo.setDoctorFullName(appointment.getDoctor().getUserAccountName());
        if (appointment.getPatient() != null) {
            appointmentInfo.setPatientId(appointment.getPatient().getUserAccountId());
            appointmentInfo.setPatientFullName(appointment.getPatient().getUserAccountName());
        }
        appointmentInfo.setAppointmentDate(appointment.getAppointmentDate());
        appointmentInfo.setAppointmentTime(DateHelper.getFormattedTime(appointment.getAppointmentDate()));
        appointmentInfo.setAppointmentFullDate(DateHelper.getFormattedDate(appointment.getAppointmentDate()));
        appointmentInfo.setIsRegistered(appointment.getIsRegistered());
        appointmentInfo.setStatus(appointment.getStatus());

        return appointmentInfo;
    }
}
