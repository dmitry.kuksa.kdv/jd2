package com.dvkuksa.model.exception;

public class InvalidDataException extends RuntimeException {

    private int code;

    public InvalidDataException(String message) {
        super(message);
        this.code = 0;
    }

    public InvalidDataException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public InvalidDataException(String message, Throwable cause) {
        super(message, cause);
        this.code = 0;
    }

    public InvalidDataException(String message, Integer code, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
