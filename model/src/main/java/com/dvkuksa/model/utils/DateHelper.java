package com.dvkuksa.model.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class DateHelper {
    public static final String UI_DATE_FORMAT_PATTERN = "EEEE, MMMM d, yyyy";
    public static final String UI_TIME_FORMAT_PATTERN = "HH:mm";
    public static final String PARSE_DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm";


    public static String getFormattedDate(LocalDate localDate) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(UI_DATE_FORMAT_PATTERN, Locale.US);
        return dateTimeFormatter.format(localDate);
    }

    public static String getFormattedDate(LocalDateTime localDateTime) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(UI_DATE_FORMAT_PATTERN, Locale.US);
        return dateTimeFormatter.format(localDateTime);
    }

    public static String getFormattedTime(LocalDateTime localDateTime) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(UI_TIME_FORMAT_PATTERN, Locale.US);
        return dateTimeFormatter.format(localDateTime);
    }

    public static List<LocalDateTime> createLocalDateList(List<String> dateTimeList) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PARSE_DATE_FORMAT_PATTERN);

        List<LocalDateTime> localDateTimeList = dateTimeList.stream()
                .map(s -> LocalDateTime.parse(s, formatter))
                .collect(Collectors.toList());

        return localDateTimeList;
    }


}
