package com.dvkuksa.model.utils;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class UiMessageHelper {
    public static Map<String, String> getErrors(BindingResult bindingResult) {
        Collector<FieldError, ?, Map<String, String>> collector = Collectors.toMap(
                fieldError -> fieldError.getField() + "Error",
                FieldError::getDefaultMessage
        );
        return bindingResult.getFieldErrors().stream().collect(collector);
    }

    public static void addErrorAlertMessage(Model model, String message){
        model.addAttribute("messageType", "danger");
        model.addAttribute("message", message);
    }

    public static void addSuccessAlertMessage(Model model, String message){
        model.addAttribute("messageType", "success");
        model.addAttribute("message", message);
    }
}
